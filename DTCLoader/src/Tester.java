
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import utilities.DaUtilities;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author eliotm
 */



public class Tester {

    private static String inDir = "\\\\denwen\\newprod_les\\files\\hostin";
    private static String outDir = "\\\\denwen\\newprod_les\\files\\hostin";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

       File matMasFolder = new File(inDir);
       File copyFolder = new File(outDir);
       File[] matMasFiles = matMasFolder.listFiles();
       BufferedReader inputStream;
       
       for (int i = 0; i < matMasFiles.length; i++) {

            if (!matMasFiles[i].isFile()) {
                continue;
            }

            File f = matMasFiles[i];            

            DaUtilities.writeMessage("Checking File: " + f.getAbsolutePath(), false);
            if (f.getName().startsWith("MATMAS") && f.getName().endsWith(".xyz")) {
                
                File g = new File(f.getAbsolutePath().substring(0, f.getAbsolutePath().indexOf(".")) + ".txt");
                f.delete();
                String origName = g.getName();
                
                if (g.renameTo(new File(outDir, g.getName() + "_" + "PROCID"))) {                    

                    g = new File(outDir, origName + "_" + "PROCID");
                    inputStream = new BufferedReader(new FileReader(copyFolder
                            + "//" + g.getName()));

                    String line;
                    while ((line = inputStream.readLine()) != null) {
                        DaUtilities.writeMessage("Line: " + line, false);
                    }
                }
            }
            else {
                DaUtilities.writeMessage("File is not MATMAS trg", false);
            }
       }
    }
}
