package utilities;
public enum QueriesEnum  {
	GET_POLICY_INFO(
			"GET_POLICY_INFO",
			"select * from poldat where polcod = 'USR' and polvar = 'DTC-LOADER'"),
	UPDATE_POLICY_INFO(
			"UPDATE_POLICY_INFO",
			"update poldat set rtstr2 = ? where polcod = ? and polvar = ? and polval = ? and srtseq = ?"),
	INSERT_PO_HEADER_2(
			"INSERT_PO_HEADER_2", 
			"insert into dtcuser.po_header( billToId, customerPoNumber,	plant, salesOrderDate, shipToName, " +
			"shipToAddress1, shipToAddress2, shipToAddress3, shipToCity, shipToState, shipToZipCode, " +
			"shipToCountry, shipToPhoneNum, shipToEmail, poType, amazonCustomerOrderNumber, billToName, " +
			"billToAddress1, billToAddress2, billToAddress3, billToCity, billToState, billToZipCode, " +
			"billToCountry, billToPhoneNum, billToEmail, marketingInsert, promoCode, promoAmount, " +
			"shipFromWarehouse, carrierType, shipToAttn, giftWrap, modifiedDate, payMethod, currency, " +
			"payCardType, cardAmount, extPrice, shipCharge, taxCharge, shipTaxTotal, vendorId, markFor, " +
			"shipVia, department, logo, subLogo, commitNumber, resvNumber, registNumber, batchNumber, " +
			"saletax, giftWrapCharge, returnTypeCode, returnAddrCode, process_YN, process_Time, buyStore, " +
			"creditTotal, custField1, custField2, custField3, custField4, custField5, custField6, " +
			"custField7, custField8, custField9, custField10, custField11, custField12, custField13, " +
			"custField14, custField15, custField16, custField17, custField18, custField19, custField20, " +
			"custServEmail, custServPhone, giftOrder, orderSubTotal, orderTotal, percentOff, percentOffDesc, " +
			"pSlipVersion, receiptId, returnName, returnAddress1, returnAddress2, returnCity, returnState, " +
			"returnZip, taxChargeB, taxChargeBDesc, taxChargeC, taxChargeCDesc, taxChargeD, taxChargeDDesc, " +
			"taxChargeDesc, taxChargeE, taxChargeEDesc, taxChargeF, taxChargeFDesc, taxTotal)" +
			"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
			"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
			"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
			"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_PO_HEADER(
			"INSERT_PO_HEADER",
			"insert into dtcuser.po_header(billtoid, customerponumber, plant, salesorderdate, "
					+ "shiptoname, shiptoaddress1, shiptoaddress2, shiptoaddress3, shiptocity, shiptostate, "
					+ "shiptozipcode, shiptocountry, potype, carriertype, buystore, amazoncustomerordernumber, "
					+ "process_time, modifieddate) "
					+ "values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"), 
	INSERT_PO_HEADER_FROM_DTC(
			"INSERT_PO_HEADER_FROM_DTC",
			"insert into dtcuser.po_header(billtoid, customerponumber, plant, modifieddate, salesorderdate," +
			"shiptoname, shiptoaddress1, shiptoaddress2, shiptoaddress3, shiptocity, shiptostate, " +
			"shiptozipcode, shiptophonenum, shiptoemail, shiptocountry, billtoname, billtoaddress1, " +
			"billtoaddress2, billtoaddress3, billtocity, billtostate, billtozipcode, billtophonenum, " +
			"billtoemail, billtocountry, promoamount, carriertype, currency, cardamount, shiptaxtotal, " +
			"shipcharge, taxcharge, extprice) " +
			"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
			"?, ?, ?, ?, ?)"),
	UPDATE_PO_HEADER1(
			"UPDATE_PO_HEADER",
			"update dtcuser.po_header set billtoname = ?, billtoaddress1 = ?, billtoaddress2 = ?, "
					+ "billtoaddress3 = ?, billtocity = ?, billtostate = ?, billtozipcode = ?, billtocountry = ?, "
					+ "billtoemail = ?, billtophonenum = ?, marketinginsert = ?, logo = ?, sublogo = ?, "
					+ "commitnumber = ?, resvnumber = ?, registnumber = ?, batchnumber = ?, giftwrapcharge = ?, "
					+ "returnaddrcode = ?, returntypecode = ?, shipfromwarehouse = ?, shiptoattn = ?, "
					+ "giftorder = ?, shipcharge = ?, taxcharge = ?, taxchargedesc = ?, modifieddate = ? "
					+ "where billtoid = ? and plant = ? and customerponumber = ?"),
	UPDATE_PO_HEADER2(
			"UPDATE_PO_HEADER2", 
			"update dtcuser.po_header set custservemail = ?, custservphone = ?, taxchargeb = ?, "
					+ "taxchargebdesc = ?, taxchargec = ?, taxchargecdesc = ?, taxcharged = ?, taxchargeddesc = ?, "
					+ "taxchargee = ?, taxchargeedesc = ?, taxchargef = ?, taxchargefdesc = ?, ordersubtotal = ?, "
					+ "ordertotal = ?, percentoff = ?, percentoffdesc = ?, taxtotal = ?, credittotal = ?, "
					+ "pslipversion = ?, returnname = ?, returnaddress1 = ?, returnaddress2 = ?, returncity = ?, "
					+ "returnstate = ?, returnzip = ?, receiptid = ?, modifieddate = ? "
					+ "where billtoid = ? and plant = ? and customerponumber = ?"),
	INSERT_PO_DETAIL( 
			"INSERT_PO_DETAIL", 
			"insert into dtcuser.po_details(billtoid, customerponumber, plant, polinenumber, "
					+ "qtyordered, unitprice, discountrate, customerpartnumber, upccode, customersellprice, "
					+ "itemtotalprice, encodedprice, colorcode, sizecode, itemtaxtype, receiptid, ranumber, "
					+ "giftwrap, modifieddate, linenumber, sku, comp) "
					+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_PO_DETAIL_FROM_DTC(
			"INSERT_PO_DETAIL_FROM_DTC",
			"insert into dtcuser.po_details(billtoid, customerponumber, plant, linenumber, qtyordered, " +
			"unitprice, sku, upccode, giftwrap, showprice, customersellprice, giftwrapcharge, " +
			"itemdescription, polinenumber, comp, modifieddate) " +
			"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_ORDER_COMMENT_DTC(
			"INSERT_ORDER_COMMENT_DTC",
			"insert into dtcuser.po_comments(billtoid, customerponumber, linenumber, sequencenumber, plant, " +
				"type, msgtext1, modifieddate) values(?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_ORDER_COMMENT(
			"INSERT_ORDER_COMMENT",
			"insert into dtcuser.po_comments(billtoid, customerponumber, "
					+ "linenumber, sequencenumber, plant, type, msgtext1, plistmsg, modifieddate ) "
					+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_DETAIL_COMMENT(
			"INSERT_DETAIL_COMMENT",
			"insert into dtcuser.po_comments(billtoid, customerponumber, "
					+ "linenumber, sequencenumber, plant, type, msgtext1, plistmsg, modifieddate) "
					+ "values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_PO_COMMENT(
			"INSERT_PO_COMMENT", 
			"insert into dtcuser.po_comments(billtoid, customerponumber, plant, sequencenumber, linenumber, " +
			"type, msgtext1, msgtext2, msgtext3, msgtext4, msgtext5, msgtext6, msgtext7,  modifieddate, " +
			"plistmsg, custfield1, custfield2, custfield3, custfield4, custfield5) " +
			"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_PO_DETAIL_2(
			"INSERT_PO_DETAILS",
			"insert into dtcuser.po_details(billToId, customerPoNumber, poLineNumber, plant, lineNumber, " +
			"qtyOrdered, unitPrice,	upcCode, customerSellPrice, itemDescription, modifiedDate, sku,	comp, " +
			"discountRate, giftNumber, giftSeqNumber, giftWrap, showPrice, giftWrapCharge, prop65, " +
			"colorCode, sizeCode, itemTaxType, receiptId, raNumber, customerPartNumber, itemTotalPrice, " +
			"encodedPrice, custField1, custField2, custField3, custField4, custField5, custField6, " +
			"custField7, custField8, custField9, custField10, custField11, custField12, custField13, " +
			"custField14, custField15, custField16, custField17, custField18, custField19, custField20)" +
			"values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
			"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	COUNT_ORDER_COMM(
			"COUNT_ORDER_COMM",
			"select count(*) rcount from dtcuser.po_comments where "
					+ "billtoid = ? and customerponumber = ? and linenumber = ? and sequencenumber = ? "
					+ "and plant = ?"),
	COUNT_CUSTOMER_PO(
			"COUNT_CUSTOMER_PO",
			"select count(*) poCount from dtcuser.po_header where billtoid = ? " 
				    + "and customerponumber = ? and plant = ?"),
	INSERT_FILE_LOG( "INSERT_FILE_LOG", "insert into dtcuser.em_file_log("
					+ "filnam, fildte, cstnam, cponum, keyval, procid, jobid, plant) values("
					+ "?, ?, ?, ?, ?, ?, ?, ?)"),
	INSERT_PROCESS_LOG( 
			"INSERT_PROCESS_LOG", 
			"insert into dtcuser.em_proc_log(procid, jobid, strdte) values(?, ?, ?)"),
	UPDATE_PROCESS_LOG( 
			"UPDATE_PROCESS_LOG", 
			"update dtcuser.em_proc_log set numfil = ?, numrow = ?, enddte = ? where procid = ? and jobid = ?"),
	INSERT_ERROR_LOG( 
			"INSERT_ERROR_LOG", 
			"insert into dtcuser.em_error_log("
					+ "filnam, errdte, cstnam, cponum, keyval, errkey, errmsg, procid, jobid) values("
					+ "?, ?, ?, ?, ?, ?, ?, ?, ?)"),
	SELECT_PO_HEADER("SELECT_PO_HEADER", 
			"select * from dtcuser.po_header poh where " +
			"trim(billtoid) = ? and customerponumber = ? and trim(plant) = ?"),
	SELECT_EM_HEADER("SELECT_EM_HEADER", 
			"select * from dtcuser.po_header emh where " +
			"billtoid = ? and customerponumber = ? and plant = ?"),
	SELECT_FILE_LOG("SELECT_FILE_LOC", 
			"select distinct cponum, cstnam, plant from dtcuser.em_file_log epl where procid = ?"),
	SELECT_PO_DETAIL("SELECT_PO_DETAIL", 
			"select * from dtcuser.po_details where " +
			"trim(billtoid) = ? and customerponumber = ? and trim(plant) = ? order by linenumber"),
	SELECT_EM_DETAIL("SELECT_EM_DETAIL", 
			"select * from dtcuser.po_details where " +
			"billtoid = ? and customerponumber = ? and plant = ? order by linenumber"),
	SELECT_PO_COMMENT("", 
			"select * from dtcuser.po_comments where " +
			"trim(billtoid) = ? and customerponumber = ? and trim(plant) = ? order by linenumber"),
	SELECT_EM_COMMENT("", 
			"select * from dtcuser.po_comments where " +
			"billtoid = ? and customerponumber = ? and plant = ? order by linenumber"),
	INSERT_UNDELIVERED_LINE("","insert into dtcuser.undelivered_lines(salesorder, customerid, plant, openqty, netprice, " +
			"created, requestdate, crval, blval) values( ?,?,?,?,?,?,?,?,?)"),
	INSERT_DELIVERED_LINE("","insert into dtcuser.delivered_lines(plant, customerPONumber, deliveryNumber, delvln) " +
			"values (?, ?, ?, ?)"),
	INSERT_CANCELED_ORDER("", "insert into usr_ord_cancel(ordnum, adddte, btcust, ordstat, prcstat) " +
			"values(?,?,?,?,?)");
	
	private String tag;
	private String query;

	QueriesEnum(String tag, String query) {
		
		this.tag = tag;
		this.query = query;
	}

	public String getTag() {		
		return tag;
	}

	public String getQuery() {
		return query;
	}
}
