package utilities.OrderCancelATron;

public enum CancelledOrderEnum {
	DISTRIBUTION_CENTER("DISTRIBUTION_CENTER", 1, 10),
	ORDER_NUMBER ("ORDER_NUMBER", 38, 10),
	ORDER_STATUS ("ORDER_STATUS", 91, 10),
	BILL_TO_CUST ("BILL_TO_CUST", 52, 10);
	
	private String tag;
	private int x;
	private int y;
	CancelledOrderEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( CancelledOrderEnum coe, String fileRow ) {
		
		return fileRow.substring(
				(coe.getX() - 1), ((coe.getX() - 1) + coe.getY())).trim();
	}
}
