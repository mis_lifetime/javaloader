package utilities.MATMAS;

public enum MaterialMasterPartEnum {
        TAG ("TAG", 1, 4),
	SEGMENT_ID   ("SEGMENT_ID", 5, 16),
	TRAN_TYPE 	("TRAN_TYPE", 21, 1),
	PART_NUMBER	("PART_NUMBER", 22, 30),
	PART_CLIENT_ID ("PART_CLIENT_ID", 52, 10),
	LONG_DESC ("LONG_DESC", 62, 60),
	UPC_CODE ("UPC_CODE", 122, 20),
	NDC_CODE ("NDC_CODE", 142, 20),
	COMODITY_CODE ("COMODITY_CODE", 162, 20),
	SUB_CONFIG  ("SUB_CONFIG", 182, 6),
	SCF_POS  ("SCF_POS", 188, 10),
	DTL_CFG  ("DTL_CFG", 198, 6),
	DCF_POS  ("DCF_POS", 204, 10),
	TYP_COD  ("TYP_COD", 214, 10),
	PART_FAM  ("PART_FAM", 224, 10),
	LOAD_LEVEL  ("LOAD_LEVEL", 234, 1),
	ORG_FLAG  ("ORG_FLAG", 235, 1),
	REV_FLAG  ("REV_FLAG", 236, 1),
	LOT_FLAG  ("LOT_FLAG", 237, 1),
	UNIT_COST  ("UNIT_COST", 238, 19),
	REO_QTY  ("REO_QTY", 257, 10),
	REO_PNT  ("REO_PNT", 267, 10),
	PACKAGE_TYPE  ("PACKAGE_TYPE", 277, 8),
	UNIT_OF_MEASURE  ("UNIT_OF_MEASURE", 285, 2),
	UNITS_PER_PACK  ("UNITS_PER_PACK", 287, 10),
	INNER_PACK_UOM  ("PACK_UOM", 297, 2),
	UNITS_PER_CASE  ("UNITS_PER_CASE", 299, 10),
	CASE_UOM  ("CASE_UOM", 309, 2),
	UNITS_PER_PALLET  ("UNITS_PER_PALLET", 311, 10),
	PALLET_UOM  ("PALLET_UOM", 321, 2),
	UNIT_LENGTH  ("UNIT_LENGTH", 323, 19),
	INNER_PACK_LENGTH  ("PACK_LENGTH", 342, 19),
	GROSS_CASE_WEIGHT  ("GROSS_WEIGHT", 361, 19),
	NET_CASE_WEIGHT  ("NET_WEIGHT", 380, 19),
	FOOTPRINT_CODE  ("FOOTPRINT_CODE", 399, 30),
	ABC_CODE  ("ABC_CODE", 429, 1),
	FIF_WIN  ("FIF_WIN", 430, 10),
	VEL_ZONE  ("VEL_ZONE", 440, 1),
	RCV_INV_STATUS  ("RCV_STATUS", 441, 1),
	RCV_INV_UOM  ("RCV_UOM", 442, 2),
	RCV_FLAG  ("RCV_FLAG", 444, 1),
	PRD_INV_FLAG  ("PRD_FLAG", 445, 1),
	LTL_CLS  ("LTL_CLASS", 446, 8),
	LAST_MOD_DATE ("MOD_DATE", 454, 8),
	LAST_MOD_USR_ID ("MOD_USR_ID", 462, 10),
        BRAND ("BRAND", 472, 4),        
        ITEM_TYPE ("ITEM_TYPE", 476, 10),
        ITEM_GROUP ("ITEM_GROUP", 486, 10),
        ITEM_CLASS ("ITEM_CLASS", 496, 10),
        BRAND_DESC ("BRAND_DESC", 506, 40),
        ITEM_TYPE_DESC ("ITEM_TYPE_DESC", 546, 25),
        ITEM_GROUP_DESC ("ITEM_GROUP_DESC", 571, 20),
        ITEM_CLASS_DESC ("ITEM_CLASS_DESC", 591, 20),
        ITEM_GROSS_WEIGHT ("ITEM_GROSS_WEIGHT", 611, 10),
        ITEM_NET_WEIGHT ("ITEM_NET_WEIGHT", 621, 10),
        ITEM_VOLUME ("ITEM_VOLUME", 631, 10),
        INNER_PACK_SCC14 ("INNER_PACK_SCC14", 641, 20),
        MASTER_PACK_SCC14 ("MASTER_PACK_SCC14", 661, 20),
        BASIC_MATERIAL ("BASIC_MATERIAL", 681, 48),
        CATEGORY_NUM ("CATEGORY_NUM", 729, 10),
        CATEGORY_DESC("CATEGORY_DESC", 739, 40),
        SUB_CATEGORY_NUM("SUB_CATEGORY_NUM", 779, 18),
        SUB_CATEGORY_DESC("SUB_CATEGORY_DESC", 797, 40);

	private String tag;
	private int x;
	private int y;
	
	MaterialMasterPartEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( MaterialMasterPartEnum mmp, String fileRow ) {
		
		return fileRow.substring(
				(mmp.getX() - 1), ((mmp.getX() - 1) + mmp.getY())).trim();
	}
}
