package utilities.MATMAS;

public enum MATMASQueriesEnum {

    INSERT_MATMASFOOT(
    "INSERT_MATMASFOOT",
    "insert into usr_matmas_footprint_seg(TAG, SEGMENT_NAME, "
    + "TRAN_TYPE, LONG_DESC, LOCALE_ID, UNIT_LENGTH, UNIT_WIDTH, "
    + "UNIT_HEIGHT, PACK_LENGTH, PACK_WIDTH, PACK_HEIGHT, "
    + "CASE_LENGTH, CASE_WIDTH, CASE_HEIGHT, CASE_LEVEL, MOD_DATE, "
    + "MOD_USR_ID, SEG_LOAD_DATE, ITEM_LOAD_DATE, PROCESSED_FLG, "
    + "PART_NUMBER, FILE_NAME ) "
    + "values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
    + "sysdate, null, 0, ?, ? )"),
    INSERT_MATMASPART(
    "INSERT_MATMASPART",
    "insert into usr_matmas_part_seg ( ITEM_VOLUME, "
    + "INNER_PACK_SCC14, MASTER_PACK_SCC14, BASIC_MATERIAL, "
    + "CATEGORY_NUM, CATEGORY_DESC, SUB_CATEGORY_NUM, SUB_CATEGORY_DESC, "
    + "ITEM_TYPE_DESC, ITEM_GROUP_DESC, ITEM_CLASS_DESC, "
    + "ITEM_GROSS_WEIGHT, ITEM_NET_WEIGHT, TAG, SEGMENT_ID, TRAN_TYPE, "
    + "PART_NUMBER, PART_CLIENT_ID, LONG_DESC, UPC_CODE, NDC_CODE, "
    + "COMODITY_CODE, SUB_CONFIG, SCF_POS, DTL_CFG, DCF_POS, TYP_COD, "
    + "PART_FAM, LOAD_LEVEL, ORG_FLAG, REV_FLAG, LOT_FLAG, UNIT_COST, "
    + "REO_QTY, REO_PNT, PACKAGE_TYPE, UNIT_OF_MEASURE, UNITS_PER_PACK, "
    + "INNER_PACK_UOM, UNITS_PER_CASE, CASE_UOM, UNITS_PER_PALLET, "
    + "PALLET_UOM, UNIT_LENGTH, INNER_PACK_LENGTH, GROSS_CASE_WEIGHT, "
    + "NET_CASE_WEIGHT, FOOTPRINT_CODE, ABC_CODE, FIF_WIN, VEL_ZONE, "
    + "RCV_INV_STATUS, RCV_INV_UOM, RCV_FLAG, PRD_INV_FLAG, LTL_CLS, "
    + "LAST_MOD_DATE, LAST_MOD_USR_ID, BRAND, ITEM_TYPE, ITEM_GROUP, "
    + "ITEM_CLASS, BRAND_DESC, FILE_NAME, SEG_LOAD_DATE, ITEM_LOAD_DATE, "
    + "PROCESSED_FLG ) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
    + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
    + "?, ?, ?, sysdate, null, 0)"),
    INSERT_MATMAS_FILE_LOG(
            "INSERT_MATMAS_FILE_LOG", 
            "insert into dtcuser.matmas_file_log(filename, prtnum, "
            + "line_length, file_size, file_status) values(?, ?, ?, ?, null)");
    
    private String tag;
    private String query;

    MATMASQueriesEnum(String tag, String query) {

        this.tag = tag;
        this.query = query;
    }

    public String getTag() {
        return tag;
    }

    public String getQuery() {
        return query;
    }
}
