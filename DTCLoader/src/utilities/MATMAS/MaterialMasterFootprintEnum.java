package utilities.MATMAS;

public enum MaterialMasterFootprintEnum {
        TAG ("TAG", 1, 9),
	SEGMENT_NAME   ("SEGMENT_NAME", 10, 11),
	TRAN_TYPE ("TRAN_TYPE", 21, 1),
	LONG_DESC ("LONG_DESC", 22, 60),
	LOCALE_ID ("LOCALE_ID", 82, 20),
	UNIT_LENGTH ("UNIT_LENGTH", 102, 19),
	UNIT_WIDTH ("UNIT_WIDTH", 121, 19),
	UNIT_HEIGHT ("UNIT_HEIGHT", 140, 19),
	PACK_LENGTH ("PACK_LENGTH", 159, 19),
	PACK_WIDTH ("PACK_WIDTH", 178, 19),
	PACK_HEIGHT ("PACK_HEIGHT", 197, 19),
	CASE_LENGTH ("CASE_LENGTH", 216, 19),
	CASE_WIDTH ("CASE_WIDTH", 235, 19),
	CASE_HEIGHT ("CASE_HEIGHT", 254, 19),
	CASE_LEVEL ("CASES_PER_LEVEL", 273, 10),
	MOD_DATE ("MOD_DATE", 283, 8),
        MOD_USR_ID ("MOD_USR_ID", 291, 10);

	private String tag;
	private int x;
	private int y;
	
	MaterialMasterFootprintEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( MaterialMasterFootprintEnum mmf, String fileRow ) {
		
		return fileRow.substring(
				(mmf.getX() - 1), ((mmf.getX() - 1) + mmf.getY())).trim();
	}
}
