package utilities.EDI754;


public enum EDI754DetailEnum {
	PARTNER_ID   ("EDI_PARTNER_ID", 1, 35),
	PARTNER_QUAL ("EDI_PARTNER_QUAL", 36, 4),
	FILLER ("FILLER", 40, 1),
	ROUTING ("ROUTING", 41, 14),
	ROUTING_REQUEST_NUMBER ("ROUTING_REQUEST_NUMBER", 55, 50),
	REC_NUMBER("REC_NUMBER", 105, 2),
	LINE_NUMBER("LINE_NUMBER", 107, 6),
	SHIP_TO_STORE_NUMBER("SHIP_TO_STORE_NUMBER", 113, 20),
	ROUTING_REQUEST_CONTROL_NBR("ROUTING_REQUEST_CONTROL_NBR", 133, 50),
	SCAC_CODE("SCAC_CODE", 183, 4),
	PICKUP_APPOINTMENT_NUMBER("PICKUP_APPOINTMENT_NUMBER", 187, 50),
	SCHEDULED_PICKUP_DATE("SCHEDULED_PICKUP_DATE", 237, 8),
	SCHEDULED_PICKUP_TIME("SCHEDULED_PICKUP_TIME", 245, 8),
	MULTI_STOP_SHIPMENT("MULTI_STOP_SHIPMENT", 253, 1),
	STOP_SEQUENCE_NUMBER("STOP_SEQUENCE_NUMBER", 254, 3),
	NUMBER_TRAILERS_FOR_PO("NUMBER_TRAILERS_FOR_PO", 257, 15),
	TRANS_SERV_LEVEL_REQ("TRANS_SERV_LEVEL_REQ", 272, 80);
	
	private String tag;
	private int x;
	private int y;
	EDI754DetailEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( EDI754DetailEnum oh, String fileRow ) {
		
		return fileRow.substring(
				(oh.getX() - 1), ((oh.getX() - 1) + oh.getY())).trim();
	}
}
