package utilities.EDI754;

public enum EDI754DetialOIDEnum {
	PARTNER_ID   ("EDI_PARTNER_ID", 1, 35),
	PARTNER_QUAL ("EDI_PARTNER_QUAL", 36, 4),
	FILLER ("FILLER", 40, 1),
	ROUTING ("ROUTING", 41, 14),
	ROUTING_REQUEST_NUMBER ("ROUTING_REQUEST_NUMBER", 55, 50),
	REC_NUMBER("REC_NUMBER", 105, 2),
	LINE_NUMBER("LINE_NUMBER", 107, 6),
	SEQ_NUMBER("SEQ_NUMBER", 113, 6),
	PO_NUMBER("PO_NUMBER", 119, 22),
	APPLICATION_ERROR_CODE("APPLICATION_ERROR_CODE", 141, 3),
	NUMBER_CARTONS("NUMBER_CARTONS", 144, 15),
	WEIGHT_IN_POUNDS("WEIGHT_IN_POUNDS", 159, 10),
	CUBIC_FEET("CUBIC_FEET", 169, 8);
	
	private String tag;
	private int x;
	private int y;
	EDI754DetialOIDEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( EDI754DetialOIDEnum oh, String fileRow ) {
		
		return fileRow.substring(
				(oh.getX() - 1), ((oh.getX() - 1) + oh.getY())).trim();
	}
}
