package utilities.EDI754;

public enum EDI754HeaderEnum {
		PARTNER_ID   ("EDI_PARTNER_ID", 1, 35),
		PARTNER_QUAL ("EDI_PARTNER_QUAL", 36, 4),
		FILLER ("FILLER", 40, 1),
		ROUTING ("ROUTING", 41, 14),
		ROUTING_REQUEST_NUMBER ("ROUTING_REQUEST_NUMBER", 55, 50),
		REC_NUMBER("REC_NUMBER", 105, 2),
		LINE_NUMBER("LINE_NUMBER", 107, 6),
		TRANSACTION_DATE("TRANSACTION_DATE", 113, 8),
		TRANSACTION_TIME("TRANSACTION_TIME", 121, 8),
		CONTACT_NAME("CONTACT_NAME", 129, 60),
		PHONE("PHONE", 189, 10),
		EMAIL("EMAIL", 199, 30),
		FAX("FAX", 229, 10),
		VENDOR_NUMBER("VENDOR_NUMBER", 239, 20),
		SHIP_FROM_NAME("SHIP_FROM_NAME", 259, 60),
		SHIP_FROM_ADDR1("SHIP_FROM_ADDR1",319, 50),
		SHIP_FROM_ADDR2("SHIP_FROM_ADDR2", 369, 50),
		SHIP_FROM_CITY("SHIP_FROM_CITY", 419, 30),
		SHIP_FROM_STATE("SHIP_FROM_STATE", 449, 2),
		SHIP_FROM_POSTAL_CODE("SHIP_FROM_POSTAL_CODE", 451, 15);
		
		
		private String tag;
		private int x;
		private int y;
		EDI754HeaderEnum( String tag, int x, int y) {
			
			this.tag = tag;
			this.x = x;
			this.y = y;
		}
		
		public String getTag() { return tag; }
		public int getX() { return x; }
		public int getY() { return y; }
		public String getValueforTag( EDI754HeaderEnum oh, String fileRow ) {
			
			return fileRow.substring(
					(oh.getX() - 1), ((oh.getX() - 1) + oh.getY())).trim();
		}
}
