package utilities.EDI754;

public enum EDI754QueriesEnum {

	INSERT_EDI754HEADER( 
			"INSERT_EDI754HEADER", 
			"insert into dtcuser.edi754header( partner_id, partner_qual, filler, routing, " +
            "routing_request_number, rec_number, line_number, transaction_date, transaction_time, contact_name, " +
            "phone, email, fax, vendor_number, ship_from_name, ship_from_addr1, ship_from_addr2, ship_from_city, " +
            "ship_from_state, ship_from_postal_code ) values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "),
    INSERT_EDI754DETAIL(
    		"INSERT_EDI754DETAIL",
    		"insert into dtcuser.edi754detail( partner_id, partner_qual, filler, routing, routing_request_number, " +
    		"rec_number, line_number, ship_to_store_number, routing_request_control_nbr, scac_code, " +
    		"pickup_appointment_number, scheduled_pickup_date, scheduled_pickup_time, multi_stop_shipment, " +
    		"stop_sequence_number, number_trailers_for_po, trans_serv_level_req ) values( ?, ?, ?, ?, ?, ?, " +
    		"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) "),
    INSERT_EDI754DETAILOID(
    		"INSERT_EDI754DETAILOID",
    		"insert into dtcuser.edi754detailoid( partner_id, partner_qual, filler, routing, routing_request_number, " +
    		"rec_number, line_number, seq_number, po_number, application_error_code, number_cartons, weight_in_pounds, " +
    		"cubic_feet ) values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )");
	
	private String tag;
	private String query;

	EDI754QueriesEnum(String tag, String query) {
		
		this.tag = tag;
		this.query = query;
	}

	public String getTag() {		
		return tag;
	}

	public String getQuery() {
		return query;
	}
}
