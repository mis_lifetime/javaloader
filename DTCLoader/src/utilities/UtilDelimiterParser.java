package utilities;

import java.util.ArrayList;

/*
 * I tried to use StringTokenizer and String.split but they did not give me the correct number
 * of tokens that were in the data line.
 */
public class UtilDelimiterParser {

	private String dataLine;
	private String delimeter;
	private ArrayList<String> tokenList;
	private ArrayList<Integer> delimeterList;
	private int readPosition;
	
	public UtilDelimiterParser(String d) {
		delimeter = d;
		
		delimeterList= new ArrayList<Integer>();
		tokenList = new ArrayList<String>();
	}
	
	public UtilDelimiterParser(String data, String d){		
		
		dataLine = data;
		delimeter = d;
		
		delimeterList= new ArrayList<Integer>();
		tokenList = new ArrayList<String>();
		
		loadTokenList();
	}
	
	public void loadDataLine(String data) {				
		dataLine = null;
		readPosition = 0;
		dataLine = data;
		loadTokenList();
	}
	
	public String getNextWidget() {
		String widget = null;
		try{
			widget = tokenList.get(readPosition).trim();
			readPosition++;
		}
		catch(Exception ex) {
			DaUtilities.writeMessage("UtilDelimiterParser (WARN): OVERREADING", true);
		}
				
		return widget;
	}
	
	private void loadTokenList() {
		
		delimeterList.clear();
		tokenList.clear();
	    for( int i = 0; i < dataLine.length(); i ++) {
	    	if( dataLine.substring(i, i+1).equals(delimeter) ) {
	    		delimeterList.add(i);
	    	}	    		
	    }
	
	    for(int i = 0; i < delimeterList.size(); i ++) {
	    	int sPos = (i==0)?0:delimeterList.get(i-1)+1;
	    	int dPos = delimeterList.get(i);	    	
	    	tokenList.add(dataLine.substring(sPos, dPos));	    	
	    }
	    
	    tokenList.add(dataLine.substring(delimeterList.get(delimeterList.size()-1)+1));
	}
}
