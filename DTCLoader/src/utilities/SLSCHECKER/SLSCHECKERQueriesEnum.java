package utilities.SLSCHECKER;

public enum SLSCHECKERQueriesEnum {
	INSERT_SLS_HEADER(
    		"INSERT_SLS_HEADER",
    		"insert into dtcuser.sls_file_log( FILE_NAME, ORDNUM, BTCUST, CPONUM, KTAG, LINE_LENGTH, FILE_SIZE )" +   
            " values( ?, ?, ?, ?, ?, ?, ? )"),
            
	INSERT_SLS_LINE_OR_NOTE(
			"INSERT_SLS_LINE_OR_NOTE",
    		"insert into dtcuser.sls_file_log( FILE_NAME, ORDNUM, ORDLIN, ORDSLN, KTAG, LINE_LENGTH, FILE_SIZE )" +   
            " values( ?, ?, ?, ?, ?, ?, ? )"),
	
	INSERT_SLS_OTHER_ENTRY(
			"INSERT_SLS_OTHER_ENTRY",
			"insert into dtcuser.sls_file_log( FILE_NAME, KTAG, LINE_LENGTH ) values ( ?, ?, ? )");
	
	private String tag;
	private String query;

	SLSCHECKERQueriesEnum(String tag, String query) {
		
		this.tag = tag;
		this.query = query;
	}

	public String getTag() {		
		return tag;
	}

	public String getQuery() {
		return query;
	}
}
