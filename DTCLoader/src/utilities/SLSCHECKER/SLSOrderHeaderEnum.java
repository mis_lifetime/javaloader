package utilities.SLSCHECKER;

public enum SLSOrderHeaderEnum {
	
	F_KEY ("F_KEY", 31, 3),
	ORDER_NUMBER ("ORDER_NUMBER", 62, 20),
	CUSTOMER_NUMBER ("CUSTOMER_NUMBER", 1314, 15),
	PO_NUMBER ("PO_NUMBER", 918, 30);
	
	private String tag;
	private int x;
	private int y;
	
	SLSOrderHeaderEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( SLSOrderHeaderEnum slsoh, String fileRow ) {
		
		return fileRow.substring(
				(slsoh.getX() - 1), ((slsoh.getX() - 1) + slsoh.getY())).trim();
	}
}
