package utilities.SLSCHECKER;

public enum SLSOrderLineEnum {

	F_KEY ("F_KEY", 31, 3),
	ORDER_NUMBER ("ORDER_NUMBER", 62, 20),
	ORDER_LINE_NUMBER("ORDER_LINE_NUMBER", 82, 4),
	ORDER_SLS_NUMBER("ORDER_SLS_NUMBER", 86, 4);
	
	private String tag;
	private int x;
	private int y;
	
	SLSOrderLineEnum( String tag, int x, int y) {
		
		this.tag = tag;
		this.x = x;
		this.y = y;
	}
	
	public String getTag() { return tag; }
	public int getX() { return x; }
	public int getY() { return y; }
	public String getValueforTag( SLSOrderLineEnum slsol, String fileRow ) {
		
		return fileRow.substring(
				(slsol.getX() - 1), ((slsol.getX() - 1) + slsol.getY())).trim();
	}
}
