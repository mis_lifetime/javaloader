package utilities.UPSInfo;

public enum UpsInfoQueriesEnum {

	INSERT_UPSINFO_PRE( 
			"INSERT_USR_UPSINFO_PRE", 
			"insert into dtcuser.usr_upsinfo_pre(country_code, postal_code_low, postal_code_high, urc) values(?, ?, ?, ?)");
	
	private String tag;
	private String query;

	UpsInfoQueriesEnum(String tag, String query) {
		
		this.tag = tag;
		this.query = query;
	}

	public String getTag() {		
		return tag;
	}

	public String getQuery() {
		return query;
	}
}
