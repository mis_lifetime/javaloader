package utilities;
public enum ErrorEnum {
	
	/* 10 chars or less for value to fit in db field */
	DB_CONNECT  ("DB_CONNECT"),
	SQL_PREPARE ("SQL_PREP"),
	DB_CLOSE ("DB_CLOSE"),
	IO_ERROR ("IO_ERROR"),
	PROCESS_LOG_START ("START_PROC"),
	FILE_LOG_INSERT ("FILELOG"),
	PO_HEADER_INSERT ("POHEADER_I"),
	PO_HEADER_UPDATE1 ("POHEAD_UP1"),
	PO_HEADER_UPDATE2 ("POHEAD_UP2"),
	FILE_COPY_ERROR ("FILE_COPY"),
	PROCESS_LOG_STOP ("STOP_PROC"),
	INSERT_POHEADER ("SECTION_10"),
	UPDATE_POEXTRA_1 ("SECTION_11"),
	UPDATE_POEXTRA_2 ("SECTION_12"),
	INSERT_PODETAIL ("SECTION_20"),
	UNKNOWN_FORMAT ("UNKOWN_FMT"),
	INSERT_OCOMMENT ("SECTION_25"),
	INSERT_DCOMMENT ("SECTION_30"),
	UNKNOWN_KEY ("UNKKNOW_KEY"),
	DUPLICATE_PO ("DUPE_PO");

	private String errorMessage;
	ErrorEnum( String em ) {
		errorMessage = em;
	}
	
	public String getErrorMessage() {
		
		if( errorMessage.length() > 10 ) { 
			errorMessage = errorMessage.substring(0, 10);
		}
		
		return errorMessage;
	}
}
