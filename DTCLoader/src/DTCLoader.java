import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import utilities.DaUtilities;
import utilities.ErrorEnum;
import constructs.AbstractPOConstruct;
import constructs.DTCPolicy;
import constructs.ProcessLogEntry;
import constructs.DTCLoader.POComment;
import constructs.DTCLoader.PODetail;
import constructs.DTCLoader.POHeader;

public class DTCLoader {
	
	private static final String PO_HEADER_DELIMITER = "|";

	private static String LOAD_FROM_DIR;
	private static String COPY_TO_DIR;
	private static String LOG_FILE_DIR;
	private static Connection conn = null;
	private static long procId;
	private static boolean isTimeToDie;
	private static HashMap<String, DTCPolicy> policyMap;

	public static void main(String args[]) throws IOException,
			InterruptedException {

		boolean isDTCInitialized = false;
		while (!isTimeToDie) {
			procId = Calendar.getInstance().getTimeInMillis();
			FileOutputStream fos = null;
			PrintStream ps = null;
			
			//fos = new FileOutputStream( ".\\Logs\\" + procId + "_runlog.txt");
			//ps = new PrintStream(fos);
			//System.setOut(ps);
			//System.setErr(ps);	
			
			//DaUtilities.writeMessage("Starting . . .", false);
			/*
			 * Get Connection, Policy Data and set up logging
			 */			
			try {
				//DaUtilities.writeMessage("Connecting . . .", false);
				conn = DaUtilities.getOracleConnection("NEWPROD");
				
				//DaUtilities.writeMessage("Settign up processing dirs . . .", false);
				// EM: use getRstst1 for test and set to getRtstr2() for live, 1 reads
				//     off my local machine if set up right so I can test
				policyMap = DaUtilities.getDTCPolicyData(conn);				
				LOAD_FROM_DIR = policyMap.get("INPUT-DIR:1").getRtstr1();
				COPY_TO_DIR = policyMap.get("OUTPUT-DIR:1").getRtstr1();
				LOG_FILE_DIR = policyMap.get("LOG-DIR:1").getRtstr1();											
				
				//DaUtilities.writeMessage("Switching output to :" + LOG_FILE_DIR, false);
				//fos.close();
				//ps.close();
				fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId + "_runlog.txt");
				ps = new PrintStream(fos);
				System.setOut(ps);
				System.setErr(ps);								
				
				DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);
				DaUtilities.writeMessage("COPY_TO_DIR: " + COPY_TO_DIR, false);
				DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);				
				
				isDTCInitialized = true;
			} catch (Exception ex) {
				DaUtilities.writeMessage("ERROR: Failed setup . . .", true);
				ex.printStackTrace();
				isDTCInitialized = false;
			}			
			
			/*
			 * Parse Files
			 */
			if( isDTCInitialized ) {
				DaUtilities.writeMessage("Parsing DTC Files . . .", false);
				try {
					parseDTCFiles();
				} catch (Exception ex) {
					DaUtilities.writeMessage("ERROR: IO Failure . . .", true);
					ex.printStackTrace();
					return;
				}
	
				/*
				 * Clean up
				 */
				DaUtilities.writeMessage("Closing Connection", false);
				try {
					conn.close();									
				} catch (Exception ex) {
					DaUtilities.writeMessage(
							"ERROR: Failed to Close Oracle Connection . . .", true);
					ex.printStackTrace();					
				}
			}
			
			conn = null;	
			policyMap = null;
			fos.close();
			ps.close();
			fos = null;
			ps = null;
			System.gc();
			
			Thread.sleep(DaUtilities.getSleepTime());
		}
	}

	private static void parseDTCFiles() throws IOException {
		File dtcFolder = new File(LOAD_FROM_DIR);
		File copyFolder = new File(COPY_TO_DIR);
		File[] dtcOrderFiles = dtcFolder.listFiles();
		BufferedReader inputStream = null;
		int numFiles = 0;
		int numRows = 0;
		int numErrs = 0;

		DaUtilities.writeMessage("Starting Process Log", false);
		ProcessLogEntry ple = new ProcessLogEntry(procId, "NA");
		ple.startLog(conn);
		
		DaUtilities.writeMessage("Processing " + dtcOrderFiles.length
				+ " files", false);
		for (int i = 0; i < dtcOrderFiles.length; i++) {

			if (!dtcOrderFiles[i].isFile())
				continue;

			File f = dtcOrderFiles[i];
			String origName = f.getName();

			if (f.renameTo(new File(COPY_TO_DIR, f.getName() + "_" + procId))) {
				DaUtilities.writeMessage("Processing File: " + origName, false);

				f = new File(COPY_TO_DIR, origName + "_" + procId);
				numFiles++;
				inputStream = new BufferedReader(new FileReader(copyFolder
						+ "//" + f.getName()));

				String line;
				while ((line = inputStream.readLine()) != null) {

					numRows++;
					boolean isOk = false;
					if(origName.startsWith("POHDR")) {						
						isOk = processPOConstruct( new POHeader(procId, "NA", origName, line, PO_HEADER_DELIMITER) );						
					} else if(origName.startsWith("PODTL")) {						
						isOk = processPOConstruct( new PODetail(procId, "NA", origName, line, PO_HEADER_DELIMITER) );						
					} else if(origName.startsWith("POCMT")) {						
						isOk = processPOConstruct( new POComment(procId, "NA", origName, line, PO_HEADER_DELIMITER) );						
					} else{
						DaUtilities.writeMessage(origName + ": is UNKNOWN . . . ", true);
					}
					
					numErrs += (isOk) ? 0 : 1;
				}

				DaUtilities.writeMessage("Closing file input stream", false);
				inputStream.close();

				DaUtilities.writeMessage("Renaming file back to orignal name",
						false);
				if (!f.renameTo(new File(COPY_TO_DIR, origName))) {
					DaUtilities.writeMessage(
							"Could not copy file back to original name",
							true);
				}
			} else {
				DaUtilities.writeMessage("Failed to Copy File", true);
			}

		}

		/*
		 * Post Last Run Information
		 */
		policyMap.get("LAST_RUN_INFO:1").setRtstr2(new Date().toString());
		policyMap.get("LAST_RUN_INFO:2").setRtstr2(String.valueOf(numFiles));
		policyMap.get("LAST_RUN_INFO:3").setRtstr2(String.valueOf(numRows));
		policyMap.get("LAST_RUN_INFO:4").setRtstr2(String.valueOf(numErrs));
		policyMap.get("LAST_RUN_INFO:1").insert(conn);
		policyMap.get("LAST_RUN_INFO:2").insert(conn);
		policyMap.get("LAST_RUN_INFO:3").insert(conn);
		policyMap.get("LAST_RUN_INFO:4").insert(conn);
		
		DaUtilities.writeMessage("Completing process log.", false);
		ple.setNumFiles(numFiles);
		ple.setNumRows(numRows);
		ple.setEndDate();
		ple.stopLog(conn);
	}
	
	private static boolean processPOConstruct(AbstractPOConstruct apoc) {
		DaUtilities.writeMessage("Processing " + apoc.getFileName() + " entry", false);
		apoc.getFileLogEntry().insert(conn);
		boolean result = apoc.insert(conn);
		if(!result) {
			apoc.getErrorLogEntry(ErrorEnum.INSERT_OCOMMENT, "Failed insert, check log").insert(conn);			
		}
		
		return result;		
	}
}
