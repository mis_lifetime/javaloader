
import constructs.EMJob;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.text.SimpleDateFormat;
import java.util.*;
import utilities.DaUtilities;

public class EMJobScheduler {

    private static final int bindPort = 15486;
    private static ServerSocket serverSocket;
    private static boolean isTimeToDie;
    private static Hashtable<String, EMJob> jobList;
    private static ArrayList<Thread> runList;
    private static Properties props;
    private static Calendar c;
    private static EMJob emj;
    private static String logDir;
    private static String logFile;
    private static PrintStream ps;

    public static void main(String[] args) throws InterruptedException {

        jobList = new Hashtable<String, EMJob>();
        runList = new ArrayList<Thread>();

        try {
            System.out.println("Binding Port : " + bindPort);
            serverSocket = new ServerSocket(bindPort);
        } catch (Exception ex) {
            System.out.println("Port Bound, Duplicate Launch exiting . . .");
            isTimeToDie = true;
            System.exit(0);
        }

        while (!isTimeToDie) {

            /*
             * Load the Properties file that contains log info and the jobs to
             * run
             */
            try {
                props = DaUtilities.getProperties("resources/JobSchedule");
            } catch (Exception ex) {
                ex.printStackTrace();
                return;
            }

            c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String s = sdf.format(c.getTime());
            FileOutputStream fos = null;

            logDir = props.getProperty("LOGDIR");
            if (logFile == null || !logFile.equals(s)) {
                logFile = sdf.format(c.getTime());

                try {
                    fos = new FileOutputStream(logDir + "/" + logFile + ".txt", true);
                } catch (IOException iox) {
                    iox.printStackTrace();
                    return;
                }

                ps = new PrintStream(fos);
                //COMMENT THESE OUT TO SEE WHATS GOING ON IN CONSOLE AND NOT IN THE LOG
                System.setOut(ps);
                System.setErr(ps);
            }
            
            DaUtilities.writeMessage("Cycle Time: " + c.getTime(), false);

            logDir = null;
            logFile = null;
            sdf = null;
            s = null;

            /*
             * Find the properties file that will run this scheduler and load
             * it. I will do this in the loop to allow changes without shutting
             * down.
             */
            DaUtilities.writeMessage("Property file entries: " + props.size(), false);

            /*
             * Load each job into a job list
             */
            if (props != null && props.size() > 0) {
                DaUtilities.writeMessage("Loading jobs to the jobList", false);
                Enumeration<Object> pElements = props.keys();
                while (pElements.hasMoreElements()) {

                    String job = pElements.nextElement().toString();
                    if (!job.equals("LOGDIR")) {
                        DaUtilities.writeMessage("Adding job: " + job, false);

                        if (!jobList.containsKey(job)) {
                            ClassLoader classLoader = EMJob.class.getClassLoader();

                            try {
                                emj = (EMJob) classLoader.loadClass(props.getProperty(job)).newInstance();
                                jobList.put(job, emj);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            classLoader = null;
                        } else {
                            try {
                                DaUtilities.writeMessage("Job already loaded, synching", false);
                                jobList.get(job).syncProperties();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    job = null;
                }
                pElements = null;
                props = null;
            } else {
                DaUtilities.writeMessage("No Jobs to load", false);
            }

            /*
             * Go through the job list to see if any jobs need to run
             */
            DaUtilities.writeMessage("Checking job run times", false);
            if (jobList != null && jobList.size() > 0) {
                Enumeration<EMJob> jobs = jobList.elements();
                while (jobs.hasMoreElements()) {

                    emj = jobs.nextElement();

                    DaUtilities.writeMessage("Checking: " + emj.getJobId(), false);
                    if (emj.canRunNow(c)) {

                        DaUtilities.writeMessage("Running: " + emj.getJobId(), false);
                        Thread t = new Thread(emj);
                        t.setName(emj.getJobId());
                        runList.add(t);
                        t.start();
                    }
                }
                jobs = null;
            } else {
                DaUtilities.writeMessage("No Jobs to check", false);
            }

            /*
             * Remove completed jobs from the list
             */
            DaUtilities.writeMessage("Checking job status in runList", false);
            if (runList != null && runList.size() > 0) {
                for (int i = 0; i < runList.size(); i++) {
                    Thread t = runList.get(i);
                    if (t.isAlive()) {
                        DaUtilities.writeMessage("Job is Running: " + t.getName(), false);
                    } else {
                        DaUtilities.writeMessage("Removing Job: " + t.getName(), false);
                        runList.remove(i);
                        t = null;
                        i--;
                    }
                }
            } else {
                DaUtilities.writeMessage("No Jobs to Check", false);
            }

            DaUtilities.writeMessage("Sleeping . . .", false);

            c = null;
            fos = null;
            ps = null;
            System.gc();

            Thread.sleep(59980);
        }
    }
}
