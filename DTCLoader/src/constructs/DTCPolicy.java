package constructs;

import java.sql.Connection;
import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

public class DTCPolicy implements Insertable {
	private String polcod;
	private String polval;
	private String polvar;
	private long srtseq;
	private String rtstr1;
	private String rtstr2;
	private long rtnum1;
	private long rtnum2;
	private float rtflt1;
	private float rtflt2;
	
	public DTCPolicy(String pcod, String pval, String pvar, 
			long sseq, String rs1, String rs2, long rn1, long rn2, 
			float rf1, float rf2 ) {
		polcod = pcod;
		polval = pval;
		polvar = pvar;
		srtseq = sseq;
		rtstr1 = rs1;
		rtstr2 = rs2;
		rtnum1 = rn1;
		rtnum2 = rn2;
		rtflt1 = rf1;
		rtflt2 = rf2;
	}

	public String getPolcod() {
		return polcod;
	}

	public String getPolval() {
		return polval;
	}

	public String getPolvar() {
		return polvar;
	}

	public long getSrtseq() {
		return srtseq;
	}

	public String getRtstr1() {
		return rtstr1;
	}

	public String getRtstr2() {
		return rtstr2;
	}

	public long getRtnum1() {
		return rtnum1;
	}

	public long getRtnum2() {
		return rtnum2;
	}

	public float getRtflt1() {
		return rtflt1;
	}

	public float getRtflt2() {
		return rtflt2;
	}

	public void setPolcod(String polcod) {
		this.polcod = polcod;
	}

	public void setPolval(String polval) {
		this.polval = polval;
	}

	public void setPolvar(String polvar) {
		this.polvar = polvar;
	}

	public void setSrtseq(long srtseq) {
		this.srtseq = srtseq;
	}

	public void setRtstr1(String rtstr1) {
		this.rtstr1 = rtstr1;
	}

	public void setRtstr2(String rtstr2) {
		this.rtstr2 = rtstr2;
	}

	public void setRtnum1(long rtnum1) {
		this.rtnum1 = rtnum1;
	}

	public void setRtnum2(long rtnum2) {
		this.rtnum2 = rtnum2;
	}

	public void setRtflt1(float rtflt1) {
		this.rtflt1 = rtflt1;
	}

	public void setRtflt2(float rtflt2) {
		this.rtflt2 = rtflt2;
	}

	@Override
	public boolean insert(Connection conn) {
				
		UtilPreparedStatement ups = new UtilPreparedStatement(QueriesEnum.UPDATE_POLICY_INFO.getQuery());
		ups.setString(1, getRtstr2());
		ups.setString(2, getPolcod());
		ups.setString(3, getPolvar());
		ups.setString(4, getPolval());
		ups.setLong(5, getSrtseq());
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}

	@Override
	public String toString() {
		
		return "POLCOD: " + polcod + "\n"
			+ "POLVAL: " + polval + "\n"
			+ "POLVAR: " + polvar + "\n"
			+ "SRTSEQ: " + srtseq + "\n"
			+ "RTSTR1: " + rtstr1 + "\n"
			+ "RTSTR2: " + rtstr2 + "\n"
			+ "RTNUM1: " + rtnum1 + "\n"
			+ "RTNUM2: " + rtnum2 + "\n"
			+ "RTFLT1: " + rtflt1 + "\n"
			+ "RTFLT2: " + rtflt2;
	}			
}
