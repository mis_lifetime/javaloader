package constructs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import utilities.DaUtilities;

public abstract class EMJob implements Runnable {
	
	private String jobId;
	private HashMap<Integer, Boolean> runMonths;
	private HashMap<Integer, Boolean> runDays;
	private HashMap<Integer, Boolean> runHours;
	private HashMap<Integer, Boolean> runMinutes;
	private String propFile;
	protected Properties jobProperties;
	
	public EMJob(String pFile) throws FileNotFoundException, IOException {
		propFile = pFile;
		syncProperties();
	}
	
	public abstract void preProcessEMJob() throws Exception;
	public abstract void postProcessEMJob() throws Exception;
	public abstract void processEMJob() throws Exception;
	
	public final void syncProperties() throws FileNotFoundException, IOException {
		jobProperties = DaUtilities.getProperties(propFile);
		jobId = jobProperties.getProperty("JOBID");
		setMonths();
		setDays();
		setHours();
		setMinutes();
	}
	
        @Override
	public void run() {
		
		try {
			preProcessEMJob();
		}
		catch(Exception ex ) {
			ex.printStackTrace();
		}
		
		try {
			processEMJob();
		}
		catch(Exception ex ) {
			ex.printStackTrace();
		}
		
		try {
			postProcessEMJob();
		}
		catch(Exception ex ) {
			ex.printStackTrace();
		}
	}
	
	public boolean canRunNow(Calendar c) {
		if( getCanRunThisMonth(c.get(Calendar.MONTH)) &&
		    getCanRunThisDay(c.get(Calendar.DAY_OF_WEEK)) &&
			getCanRunThisHour(c.get(Calendar.HOUR_OF_DAY)) &&
			getCanRunThisMinute(c.get(Calendar.MINUTE))) {
				
			return true;
		}
		else {
			return false;
		}
	}
	
	public String getJobId() {
		return jobId;
	}
	
	private boolean getCanRunThisMonth(int m) {
		return runMonths.get(new Integer(m)).booleanValue();
	}
	
	private boolean getCanRunThisDay(int d) {
		return runDays.get(new Integer(d)).booleanValue();
	}
	
	private boolean getCanRunThisHour(int h) {
		return runHours.get(new Integer(h)).booleanValue();
	}
	
	private boolean getCanRunThisMinute(int m) {
		return runMinutes.get(new Integer(m)).booleanValue();
	}
	
	private void setMonths() {
		String months = jobProperties.getProperty("RUNMONTHS");
		runMonths = new HashMap<Integer, Boolean>();
		boolean isOn = false;
		
		if(months.equals("*")) isOn = true;	
			
		setUpHashMap(0, 12, isOn, months, runMonths);
	}
	
	private void setDays() {
		String days = jobProperties.getProperty("RUNDAYS");
		runDays = new HashMap<Integer, Boolean>();
		boolean isOn = false;
		
		if(days.equals("*")) isOn = true;	
			
		setUpHashMap(1, 8, isOn, days, runDays);
	}
	
	private void setHours() {
		String hours = jobProperties.getProperty("RUNHOURS");
		runHours = new HashMap<Integer, Boolean>();
		boolean isOn = false;
		
		if(hours.equals("*")) isOn = true;	
			
		setUpHashMap(0, 24, isOn, hours, runHours);
	}
	
	private void setMinutes() {
		String minutes = jobProperties.getProperty("RUNMINUTES");
		runMinutes = new HashMap<Integer, Boolean>();
		boolean isOn = false;
		
		if(minutes.equals("*")) isOn = true;	
			
		setUpHashMap(0, 60, isOn, minutes, runMinutes);
	}
	
	private void setUpHashMap( int lowBound, int highBound, boolean b, String sMap, 
			HashMap<Integer, Boolean> h ) {
		for(int i = lowBound; i < highBound; i++) {
			h.put(new Integer(i), new Boolean(b));				
		}
		
		if(!b && sMap != null) {
			String[] s = sMap.split(",");
			for(int i = 0; i < s.length; i++) {
				Integer m = new Integer(s[i]);
				h.remove(m);
				h.put(m, true);
			}
		}
	}
}
