package constructs.OrderCancelATron;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;

import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;
import utilities.OrderCancelATron.CancelledOrderEnum;
import constructs.Insertable;

public class CancelledOrder implements Insertable {

	HashMap<String, String> cancelledOrderMap;
	private String distributionCenter;
	private String orderNumber;
	private String billToCustomer;
	private String orderStatus;
	
	public CancelledOrder( String data ) {
		
		cancelledOrderMap = new HashMap<String, String>();
		
		for (CancelledOrderEnum he : CancelledOrderEnum.values()) {
			cancelledOrderMap.put(he.getTag(), he.getValueforTag(he, data));
		}
		
		setDistributionCenter();
		setOrderNumber();
		setBillToCustomer();
		setOrderStatus();
	}
	
	public String getDistributionCenter() {
		return distributionCenter;
	}
	
	public String getOrderNumber() {
		return orderNumber;
	}
	
	public String getBillToCustomer() {
		return billToCustomer;
	}
	
	public String getOrderStatus() {
		return orderStatus;
	}
	
	/* Setters */
	private void setDistributionCenter() {
		distributionCenter = cancelledOrderMap.get(CancelledOrderEnum.DISTRIBUTION_CENTER.getTag());
	}
	
	private void setOrderNumber() {
		orderNumber = cancelledOrderMap.get(CancelledOrderEnum.ORDER_NUMBER.getTag());
	}
	
	private void setBillToCustomer() {
		billToCustomer = cancelledOrderMap.get(CancelledOrderEnum.BILL_TO_CUST.getTag());
	}
	
	private void setOrderStatus() {
		orderStatus = cancelledOrderMap.get(CancelledOrderEnum.ORDER_STATUS.getTag());
	}
	
	@Override
	public boolean insert(Connection conn) {
		UtilPreparedStatement ups = new UtilPreparedStatement(QueriesEnum.INSERT_CANCELED_ORDER.getQuery());
		ups.setString(1, getOrderNumber());
		ups.setDate(2, new Date());
		ups.setString(3, getBillToCustomer());
		ups.setString(4, getOrderStatus());
		ups.setInt(5, 1);
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}

	@Override
	public String toString() {
		
		return "DC: " + distributionCenter + "\n" + 
			"Order: " + orderNumber + "\n" + 
			"BillTo: " + billToCustomer + "\n" + 
			"Status: " + orderStatus;
	}	
}