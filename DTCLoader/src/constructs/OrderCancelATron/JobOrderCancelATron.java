package constructs.OrderCancelATron;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;

import utilities.DaUtilities;
import constructs.EMJob;
import constructs.ProcessLogEntry;

public class JobOrderCancelATron extends EMJob {
	private Connection dtcConnection;
	private static String LOAD_FROM_DIR;
	private static String COPY_TO_DIR;
	private static String LOG_FILE_DIR;
        private static String CONNECTION_SITE;
        private static String DISTRO_KEY;
	private static long procId;
	//private FileOutputStream fos = null;
	//private PrintStream ps = null;
	
	public JobOrderCancelATron() throws FileNotFoundException,
		IOException {
		super("resources/OrderCancelATronProperties");					
	}
	
	@Override
	public void postProcessEMJob() throws Exception {
		DaUtilities.writeMessage("Closing files and connections", false);
		dtcConnection.close();
		dtcConnection = null;	
		//fos.close();
		//ps.close();
		//fos = null;
		//ps = null;
		System.gc();
	}

	@Override
	public void preProcessEMJob() throws Exception {
		procId = Calendar.getInstance().getTimeInMillis();	
		LOAD_FROM_DIR = jobProperties.getProperty("INPUTDIR");
		COPY_TO_DIR = jobProperties.getProperty("OUTPUTDIR");
		LOG_FILE_DIR = jobProperties.getProperty("LOGDIR");
                CONNECTION_SITE = jobProperties.getProperty("CONNECTION");
                
                if(CONNECTION_SITE.equals("FONPROD")  || CONNECTION_SITE.equals("FONTEST")) {
                    DISTRO_KEY = "MIRALOMA";
                }
                else {
                    DISTRO_KEY = "D006";
                }
		
                DaUtilities.writeMessage("CONNECTION = " + CONNECTION_SITE, false);
                DaUtilities.writeMessage("DISTRO_KEY = " + DISTRO_KEY, false);
		dtcConnection = DaUtilities.getOracleConnection(jobProperties.getProperty("CONNECTION"));
		//fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId + "_runlog.txt");
				
		//ps = new PrintStream(fos);
		//System.setOut(ps);
		//System.setErr(ps);
		
		DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);
		DaUtilities.writeMessage("COPY_TO_DIR: " + COPY_TO_DIR, false);
		DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);
		DaUtilities.writeMessage(dtcConnection.toString(), false);
	}

	@Override
	public void processEMJob() throws Exception {
		File loadFolder = new File(LOAD_FROM_DIR);
		File copyFolder = new File(COPY_TO_DIR);
		File[] readFile = loadFolder.listFiles();
		BufferedReader inputStream = null;
		int numFiles = 0;
		int numRows = 0;
		int numErrs = 0;

		DaUtilities.writeMessage("Starting Process Log", false);
		ProcessLogEntry ple = new ProcessLogEntry(procId, super.getJobId());
		ple.startLog(dtcConnection);
		
		if(readFile != null) {
			for (int i = 0; i < readFile.length; i++) {
	
				if (!readFile[i].isFile())
					continue;
	
				File f = readFile[i];
				String origName = f.getName();
	
				if (f.renameTo(new File(COPY_TO_DIR, f.getName() + "_" + procId))) {
					DaUtilities.writeMessage("Processing File: " + origName, false);
	
					f = new File(COPY_TO_DIR, origName + "_" + procId);
					numFiles++;
					inputStream = new BufferedReader(new FileReader(copyFolder
							+ "//" + f.getName()));
	
					String line;								
					while ((line = inputStream.readLine()) != null) {
	
						numRows++;					
						boolean isOk = false;								
						
						CancelledOrder co = new CancelledOrder(line);                                                
						if(co.getDistributionCenter().equals(DISTRO_KEY)) {                                                        
							co.insert(dtcConnection);
						}                                               
                                                
						isOk = true;					
						numErrs += (isOk) ? 0 : 1;
					}
	
					DaUtilities.writeMessage("Closing file input stream", false);
					inputStream.close();
	
					DaUtilities.writeMessage("Renaming file back to orignal name",
							false);
					if (!f.renameTo(new File(COPY_TO_DIR, origName))) {
						DaUtilities.writeMessage(
								"Could not copy file back to original name",
								true);
					}
				} else {
					DaUtilities.writeMessage("Failed to Copy File", true);
				}
			}
		}
		
		DaUtilities.writeMessage("Completing process log.", false);
		ple.setNumFiles(numFiles);
		ple.setNumRows(numRows);
		ple.setEndDate();
		ple.stopLog(dtcConnection);
	}
}
