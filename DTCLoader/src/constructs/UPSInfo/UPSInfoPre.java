package constructs.UPSInfo;

import java.sql.Connection;
import java.util.HashMap;

import constructs.AbstractPOConstruct;

import utilities.DaUtilities;
import utilities.UtilPreparedStatement;
import utilities.UPSInfo.UpsInfoQueriesEnum;

public class UPSInfoPre extends AbstractPOConstruct {

	private String countryCode;
	private String postalCodeLow;
	private String postalCodeHigh;
	private String urc;
	
	public UPSInfoPre(long pid, String jid, String fName, String dataLine,
			String delimiter) {
		super(pid, jid, fName, dataLine, delimiter);
		
		countryCode = dParse.getNextWidget().replaceAll("\"", "");
		postalCodeLow = dParse.getNextWidget().replaceAll("\"", "");
		postalCodeHigh = dParse.getNextWidget().replaceAll("\"", "");
		urc = dParse.getNextWidget().replaceAll("\"", "");				
	}
	
	public String getCountryCode() {
		return countryCode;
	}

	public String getPostalCodeLow() {
		return postalCodeLow;
	}

	public String getPostalCodeHigh() {
		return postalCodeHigh;
	}

	public String getUrc() {
		return urc;
	}

	@Override
	protected void loadDtcTableMap() {
		dtcTableMap = new HashMap<String, String>();
		dtcTableMap.put("countryCode", countryCode);
		dtcTableMap.put("postalCodeLow", postalCodeLow);
		dtcTableMap.put("postalCodeHigh", postalCodeHigh);
		dtcTableMap.put("urc", urc);
	}

	@Override
	public boolean insert(Connection conn) {		
		UtilPreparedStatement ups = new UtilPreparedStatement(UpsInfoQueriesEnum.INSERT_UPSINFO_PRE.getQuery());
		ups.setString(1, getCountryCode());
		ups.setString(2, getPostalCodeLow());
		ups.setString(3, getPostalCodeHigh());
		ups.setString(4, getUrc());
		
		return DaUtilities.executeUpdate(conn, ups, false);
	}
}
