package constructs;

import java.sql.Connection;
import java.util.Date;

import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

public class FileLogEntry {

	private long procId; 
	private String jobId;
	private String fileName; 
	private String custName;
	private String cpoNum; 
	private String keyVal; 
	private String plantId;
	
	public FileLogEntry( long pId, String jid, String kVal, String fName, String cName, String cNum, String pltId ) {
		procId = pId;
		jobId = jid;
		fileName = fName;
		custName = cName;
		cpoNum = cNum;
		keyVal = kVal;
		plantId = pltId;
	}
	
	public boolean insert(Connection conn) {
		Date insertDate = new Date();
		UtilPreparedStatement upsFileLogInsert = new UtilPreparedStatement(
				QueriesEnum.INSERT_FILE_LOG.getQuery());
		upsFileLogInsert.reset();
		upsFileLogInsert.setString(1, fileName);
		upsFileLogInsert.setTimestamp(2, new java.sql.Timestamp(insertDate
				.getTime()));
		upsFileLogInsert.setString(3, custName);
		upsFileLogInsert.setString(4, cpoNum);
		upsFileLogInsert.setString(5, keyVal);
		upsFileLogInsert.setLong(6, procId);
		upsFileLogInsert.setString(7, jobId);
		upsFileLogInsert.setString(8, plantId);

		return DaUtilities.executeUpdate(conn, upsFileLogInsert, true);
	}
}
