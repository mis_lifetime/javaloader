package constructs;

import java.sql.Connection;
import java.util.Date;

import utilities.DaUtilities;
import utilities.ErrorEnum;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

public class ErrorLogEntry {
	private long processId;
	private String fileName; 
	private String billToId; 
	private String cpoNum;
	private String keyVal; 
	private ErrorEnum errorEnumVal; 
	private String message;
	private String jobId;
	
	public ErrorLogEntry( long pid, String jid, String fName, String btid, String cNum, String kVal, ErrorEnum ee, String msg ) {
		processId = pid;
		jobId = jid;
		fileName = fName;
		billToId = btid;
		cpoNum = cNum;
		keyVal = kVal;
		errorEnumVal = ee;
		message = msg;
	}
	
	public boolean insert(Connection conn) {
		UtilPreparedStatement upsErrorLogInsert = new UtilPreparedStatement(
				QueriesEnum.INSERT_ERROR_LOG.getQuery());
		Date errorDate = new Date();

		if (message.length() > 200) {
			message = message.substring(0, 200);
		}

		upsErrorLogInsert.reset();
		upsErrorLogInsert.setString(1, fileName);
		upsErrorLogInsert.setTimestamp(2, new java.sql.Timestamp(errorDate
				.getTime()));
		upsErrorLogInsert.setString(3, billToId);
		upsErrorLogInsert.setString(4, cpoNum);
		upsErrorLogInsert.setString(5, keyVal);
		upsErrorLogInsert.setString(6, errorEnumVal.getErrorMessage());
		upsErrorLogInsert.setString(7, message);
		upsErrorLogInsert.setLong(8, processId);
		upsErrorLogInsert.setString(9, jobId);

		return DaUtilities.executeUpdate(conn, upsErrorLogInsert, true);
	}
}
