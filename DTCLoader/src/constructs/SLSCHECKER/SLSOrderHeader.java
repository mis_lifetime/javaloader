package constructs.SLSCHECKER;

import java.util.HashMap;
import java.util.Iterator;

import utilities.MATMAS.MaterialMasterPartEnum;
import utilities.SLSCHECKER.SLSOrderHeaderEnum;

public class SLSOrderHeader {

	HashMap<String, String> orderMap;
	
	public SLSOrderHeader( String data ) {
	
		orderMap = new HashMap<String, String>();
		for (SLSOrderHeaderEnum slsOrd : SLSOrderHeaderEnum.values()) {
			
			orderMap.put(slsOrd.getTag(), slsOrd.getValueforTag(slsOrd, data));
		}
	}
	
	public String getOrderNumber() {
		return orderMap.get(SLSOrderHeaderEnum.ORDER_NUMBER.getTag());
	}
	
	public String getCustomerNumber() {
		return orderMap.get(SLSOrderHeaderEnum.CUSTOMER_NUMBER.getTag());
	}

	public String getPONumber() {
		return orderMap.get(SLSOrderHeaderEnum.PO_NUMBER.getTag());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		StringBuffer sb = new StringBuffer();
		
		Iterator<String> i = orderMap.keySet().iterator();
		while( i.hasNext() ) {
			String key = i.next();
            sb.append(key);
            sb.append(" : ");
            sb.append(orderMap.get(key));
            sb.append("\n");
		}
		
		return sb.toString();
	}
}
