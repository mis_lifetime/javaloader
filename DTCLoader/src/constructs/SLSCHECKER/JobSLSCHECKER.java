package constructs.SLSCHECKER;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;

import utilities.DaUtilities;
import utilities.UtilPreparedStatement;
import utilities.SLSCHECKER.SLSCHECKERQueriesEnum;
import constructs.EMJob;
import constructs.ProcessLogEntry;

public class JobSLSCHECKER extends EMJob {

    private Connection dtcConnection;
    private static String LOAD_FROM_DIR;
    private static String COPY_TO_DIR;
    private static String LOG_FILE_DIR;
    private static long procId;

    public JobSLSCHECKER() throws FileNotFoundException, IOException {
        super("resources/SLSCHECKERProperties");
    }

    @Override
    public void postProcessEMJob() throws Exception {
        DaUtilities.writeMessage("Closing files and connections", false);
        dtcConnection.close();
        dtcConnection = null;
        // fos.close();
        // ps.close();
        // fos = null;
        // ps = null;
        System.gc();
    }

    @Override
    public void preProcessEMJob() throws Exception {
        procId = Calendar.getInstance().getTimeInMillis();
        LOAD_FROM_DIR = jobProperties.getProperty("INPUTDIR");
        COPY_TO_DIR = jobProperties.getProperty("OUTPUTDIR");
        LOG_FILE_DIR = jobProperties.getProperty("LOGDIR");

        dtcConnection = DaUtilities.getOracleConnection(jobProperties.getProperty("CONNECTION"));
        // fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId +
        // "_runlog.txt");

        // ps = new PrintStream(fos);
        // System.setOut(ps);
        // System.setErr(ps);

        DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);
        DaUtilities.writeMessage("COPY_TO_DIR: " + COPY_TO_DIR, false);
        DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);
        DaUtilities.writeMessage(dtcConnection.toString(), false);
    }

    @Override
    public void processEMJob() throws Exception {
        File slsFolder = new File(LOAD_FROM_DIR);
        File copyFolder = new File(COPY_TO_DIR);
        File[] slsFiles = slsFolder.listFiles();
        BufferedReader inputStream = null;
        int numFiles = 0;
        int numRows = 0;
        int numErrs = 0;

        DaUtilities.writeMessage("Starting Process Log", false);
        ProcessLogEntry ple = new ProcessLogEntry(procId, super.getJobId());
        ple.startLog(dtcConnection);

        for (File f : slsFiles) {

            if (f.isFile() && f.getName().startsWith("SLS")) {
                String origName = f.getName();

                if (f.renameTo(new File(COPY_TO_DIR, f.getName() + "_" + procId))) {
                    DaUtilities.writeMessage("Processing File: " + origName, false);


                    f = new File(COPY_TO_DIR, origName + "_" + procId);
                    numFiles++;
                    inputStream = new BufferedReader(new FileReader(copyFolder
                            + "//" + f.getName()));

                    String line;
                    while ((line = inputStream.readLine()) != null) {

                        numRows++;
                        boolean isOk = false;

                        String keyVal = line.substring(30, 34).trim();
                        System.out.println(keyVal + " : " + origName);

                        if (keyVal.equals("AHS")) {
                            SLSOrderHeader slsOrder = new SLSOrderHeader(line);
                            //System.out.println(slsOrder.toString());

                            UtilPreparedStatement ups = new UtilPreparedStatement(SLSCHECKERQueriesEnum.INSERT_SLS_HEADER.getQuery());
                            ups.setString(1, origName);
                            ups.setString(2, slsOrder.getOrderNumber());
                            ups.setString(3, slsOrder.getCustomerNumber());
                            ups.setString(4, slsOrder.getPONumber());
                            ups.setString(5, keyVal);
                            ups.setInt(6, line.length());
                            ups.setLong(7, f.length());

                            DaUtilities.executeUpdate(dtcConnection, ups, false);
                        } else {
                            SLSOrderLine slsLine = new SLSOrderLine(line);
                            //System.out.println(slsLine.toString());

                            UtilPreparedStatement ups = new UtilPreparedStatement(SLSCHECKERQueriesEnum.INSERT_SLS_LINE_OR_NOTE.getQuery());
                            ups.setString(1, origName);
                            ups.setString(2, slsLine.getOrderNumber());
                            ups.setString(3, slsLine.getOrderLine());
                            ups.setString(4, slsLine.getOrderSLS());
                            ups.setString(5, keyVal);
                            ups.setInt(6, line.length());
                            ups.setLong(7, f.length());

                            DaUtilities.executeUpdate(dtcConnection, ups, false);
                        }

                        isOk = true;

                        numErrs += (isOk) ? 0 : 1;
                    }

                    DaUtilities.writeMessage("Closing file input stream", false);
                    inputStream.close();

                    DaUtilities.writeMessage("Renaming file back to orignal name",
                            false);
                    if (!f.renameTo(new File(COPY_TO_DIR, origName))) {
                        DaUtilities.writeMessage(
                                "Could not copy file back to original name", true);
                    }
                } else {
                    DaUtilities.writeMessage("Failed to Copy File", true);
                }

                dtcConnection.commit();
            }
        }

        dtcConnection.commit();

        DaUtilities.writeMessage("Completing process log.", false);
        ple.setNumFiles(numFiles);
        ple.setNumRows(numRows);
        ple.setEndDate();
        ple.stopLog(dtcConnection);
    }
}
