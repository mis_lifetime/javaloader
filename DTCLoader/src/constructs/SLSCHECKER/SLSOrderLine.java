package constructs.SLSCHECKER;

import java.util.HashMap;
import java.util.Iterator;

import utilities.MATMAS.MaterialMasterPartEnum;
import utilities.SLSCHECKER.SLSOrderLineEnum;

public class SLSOrderLine {

HashMap<String, String> lineMap;
	
	public SLSOrderLine( String data ) {
	
		lineMap = new HashMap<String, String>();
		for (SLSOrderLineEnum slsLine : SLSOrderLineEnum.values()) {
			
			lineMap.put(slsLine.getTag(), slsLine.getValueforTag(slsLine, data));
		}
	}
	
	public String getOrderNumber() {
		return lineMap.get(SLSOrderLineEnum.ORDER_NUMBER.getTag());
	}

	public String getOrderLine() {
		return lineMap.get(SLSOrderLineEnum.ORDER_LINE_NUMBER.getTag());
	}
	
	public String getOrderSLS() {
		return lineMap.get(SLSOrderLineEnum.ORDER_SLS_NUMBER.getTag());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		StringBuffer sb = new StringBuffer();
		
		Iterator<String> i = lineMap.keySet().iterator();
		while( i.hasNext() ) {
			String key = i.next();
            sb.append(key);
            sb.append(" : ");
            sb.append(lineMap.get(key));
            sb.append("\n");
		}
		
		return sb.toString();
	}
}
