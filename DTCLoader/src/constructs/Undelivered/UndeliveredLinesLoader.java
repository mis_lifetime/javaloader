package constructs.Undelivered;

import constructs.EMJob;
import constructs.ProcessLogEntry;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import utilities.DaUtilities;

public class UndeliveredLinesLoader extends EMJob {

	private Connection dtcConnection;
	private static final String UNDLVRDLINES_DELIMETER = "\t";
	private static String LOAD_FROM_DIR;	
	private static String LOG_FILE_DIR;
	private static long procId;
        private static String CONNECTION_SITE;
        private static String DISTRO_KEY;
	//private FileOutputStream fos = null;
	//private PrintStream ps = null;
	
	public UndeliveredLinesLoader() throws FileNotFoundException,
			IOException {
		super("resources/UndeliveredLinesProperties");	
	}

	@Override
	public void postProcessEMJob() throws SQLException, IOException {
		
		DaUtilities.writeMessage("Closing files and connections", false);
		dtcConnection.close();
		dtcConnection = null;	
		//fos.close();
		//ps.close();
		//fos = null;
		//ps = null;
		System.gc();
	}

	@Override
	public void preProcessEMJob() throws IOException, SQLException, ClassNotFoundException {
		procId = Calendar.getInstance().getTimeInMillis();	
		LOAD_FROM_DIR = jobProperties.getProperty("INPUTDIR");		
		LOG_FILE_DIR = jobProperties.getProperty("LOGDIR");
		CONNECTION_SITE = jobProperties.getProperty("CONNECTION");
                
                if(CONNECTION_SITE.equals("FONPROD")  || CONNECTION_SITE.equals("FONTEST")) {
                    DISTRO_KEY = "SYML";
                }
                else {
                    DISTRO_KEY = "1600";
                }
                
                DaUtilities.writeMessage("DISTRO_KEY = " + DISTRO_KEY, false);
		dtcConnection = DaUtilities.getOracleConnection(jobProperties.getProperty("CONNECTION"));
		//fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId + "_runlog.txt");
				
		//ps = new PrintStream(fos);
		//System.setOut(ps);
		//System.setErr(ps);		                
                
		DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);		
		DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);
		DaUtilities.writeMessage(dtcConnection.toString(), false);	
		
		DaUtilities.writeMessage("Truncating tables . . .", false);	
		Statement st = dtcConnection.createStatement();
		try {
			st.executeUpdate("truncate table dtcuser.undelivered_lines");
			st.executeUpdate("truncate table dtcuser.delivered_lines");
		}
		catch( Exception ex ) {
			DaUtilities.writeMessage("Failed: " + ex.toString(), true);
			ex.printStackTrace();
		}
		finally {
			st.close();
			st = null;
		}
	}

	@Override
	public void processEMJob() throws Exception {
		File undlvFolder = new File(LOAD_FROM_DIR);		
		File[] undlvLinesFile = undlvFolder.listFiles();
		BufferedReader inputStream = null;
		int numFiles = 0;
		int numRows = 0;
		

		DaUtilities.writeMessage("Starting Process Log", false);
		ProcessLogEntry ple = new ProcessLogEntry(procId, super.getJobId());
		ple.startLog(dtcConnection);
		
		DaUtilities.writeMessage("Processing " + undlvLinesFile.length
				+ " files", false);
		for (int i = 0; i < undlvLinesFile.length; i++) {

			if (!undlvLinesFile[i].isFile())
				continue;

			File f = undlvLinesFile[i];
			String origName = f.getName();
			
			DaUtilities.writeMessage("Processing File: " + origName, false);
			
			numFiles++;
			inputStream = new BufferedReader(new FileReader(LOAD_FROM_DIR
					+ "//" + f.getName()));
			
			String line;
			UndeliveredLine ul = new UndeliveredLine(UNDLVRDLINES_DELIMETER);
			DeliveredLine dl = new DeliveredLine(UNDLVRDLINES_DELIMETER);				
			
			int undlvCnt = 0, dlvCnt = 0;
			while ((line = inputStream.readLine()) != null) {

				numRows++;					
					
				if(f.getName().startsWith("UN")) {
					ul.load(line);
					if(ul.getPlant().equals(DISTRO_KEY) && ul.getOpenQtyIntVal() > 0) { 
						undlvCnt ++;	
						ul.insert(dtcConnection);
						ul.reset();
						
						if(undlvCnt%5000 == 0) {
							ul.executeBatch();
							dtcConnection.commit();
							DaUtilities.writeMessage("UNDLV: " + undlvCnt, false);
						}
						
					}
				}
				else if(f.getName().startsWith("DEL")) {
					dl.load(line);
					if(dl.getPlant().equals(DISTRO_KEY)) {
						dlvCnt ++;
						dl.insert(dtcConnection);
						dl.reset();
					
						if(dlvCnt%5000 == 0) {
							dl.executeBatch();
							dtcConnection.commit();
							DaUtilities.writeMessage("DLV: " + dlvCnt, false);
						}
					}
				}												
			}
				
			DaUtilities.writeMessage("UNDLV: " + undlvCnt, false);
			if(undlvCnt > 0) {
				ul.executeBatch();
				dtcConnection.commit();
			}
			
			DaUtilities.writeMessage("DLV: " + dlvCnt, false);
			if(dlvCnt > 0) {
				dl.executeBatch();
				dtcConnection.commit();
			}
			
			ul = null;
			dl = null;
			
			DaUtilities.writeMessage("Closing file input stream", false);
			inputStream.close();
			//outputStream.close();							
		}

		DaUtilities.writeMessage("Completing process log.", false);
		ple.setNumFiles(numFiles);
		ple.setNumRows(numRows);
		ple.setEndDate();
		ple.stopLog(dtcConnection);		
	}
}
