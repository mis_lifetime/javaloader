package constructs.Undelivered;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utilities.QueriesEnum;
import utilities.UtilDelimiterParser;

public class DeliveredLine {

	private PreparedStatement pst;
	private UtilDelimiterParser dParse;
	private String plant;
	private String loc;	 
	private String customer;	          
	private String customerName;	   
	private String customerPONumber;	
	private String salesOrder;	
	private String salesOrderLine;	
	private String deliveryNumber;	
	private String delvln;
	private String cat;	
	private String reqDate;	
	private String canDatep;	
	private String profitCenter;	
	private String prodHierchy;	     
	private String prodHierchyDesc;
	private String itemNumber;
	private String delQty;
	private String uom;
	private String batch;	
	private String netPrice;	
	private String extPrice;	
	private String extCost;	
	private String grMargin;	
	private String gmPct;
	private String pt;
	private String created;
	private String bi;
	private String gi;
	private String offc;
	private String za;
	private String misc;
	
	public DeliveredLine( String d ) {
		dParse = new UtilDelimiterParser(d);
	}

	public void load(String data) {
		
		dParse.loadDataLine(data);
		
		plant = dParse.getNextWidget();
		loc = dParse.getNextWidget();	 
		customer = dParse.getNextWidget();	          
		customerName = dParse.getNextWidget();	   
		customerPONumber = dParse.getNextWidget();	
		salesOrder = dParse.getNextWidget();	
		salesOrderLine = dParse.getNextWidget();	
		deliveryNumber = dParse.getNextWidget();	
		delvln = dParse.getNextWidget();
		cat = dParse.getNextWidget();	
		reqDate = dParse.getNextWidget();	
		canDatep = dParse.getNextWidget();	
		profitCenter = dParse.getNextWidget();	
		prodHierchy = dParse.getNextWidget();	     
		prodHierchyDesc = dParse.getNextWidget();
		itemNumber = dParse.getNextWidget();
		delQty = dParse.getNextWidget();
		uom = dParse.getNextWidget();
		batch = dParse.getNextWidget();	
		netPrice = dParse.getNextWidget();	
		extPrice = dParse.getNextWidget();	
		extCost = dParse.getNextWidget();	
		grMargin = dParse.getNextWidget();	
		gmPct = dParse.getNextWidget();
		pt = dParse.getNextWidget();
		created = dParse.getNextWidget();
		bi = dParse.getNextWidget();
		gi = dParse.getNextWidget();
		offc = dParse.getNextWidget();
		za = dParse.getNextWidget();
		misc = dParse.getNextWidget();
	}
	
	public void reset() {
		plant = null;
		loc = null;	 
		customer = null;	          
		customerName = null;	   
		customerPONumber = null;	
		salesOrder = null;	
		salesOrderLine = null;	
		deliveryNumber = null;	
		delvln = null;
		cat = null;	
		reqDate = null;	
		canDatep = null;	
		profitCenter = null;	
		prodHierchy = null;	     
		prodHierchyDesc = null;
		itemNumber = null;
		delQty = null;
		uom = null;
		batch = null;	
		netPrice = null;	
		extPrice = null;	
		extCost = null;	
		grMargin = null;	
		gmPct = null;
		pt = null;
		created = null;
		bi = null;
		gi = null;
		offc = null;
		za = null;
		misc = null;
	}
	
	public String getPlant() {
		return plant;
	}

	public String getLoc() {
		return loc;
	}

	public String getCustomer() {
		return customer;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getCustomerPONumber() {
		return customerPONumber;
	}

	public String getSalesOrder() {
		return salesOrder;
	}

	public String getSalesOrderLine() {
		return salesOrderLine;
	}

	public String getDeliveryNumber() {
		return deliveryNumber;
	}

	public String getDelvln() {
		return delvln;
	}

	public String getCat() {
		return cat;
	}

	public String getReqDate() {
		return reqDate;
	}

	public String getCanDatep() {
		return canDatep;
	}

	public String getProfitCenter() {
		return profitCenter;
	}

	public String getProdHierchy() {
		return prodHierchy;
	}

	public String getProdHierchyDesc() {
		return prodHierchyDesc;
	}

	public String getItemNumber() {
		return itemNumber;
	}

	public String getDelQty() {
		return delQty;
	}

	public String getUom() {
		return uom;
	}

	public String getBatch() {
		return batch;
	}

	public String getNetPrice() {
		return netPrice;
	}

	public String getExtPrice() {
		return extPrice;
	}

	public String getExtCost() {
		return extCost;
	}

	public String getGrMargin() {
		return grMargin;
	}

	public String getGmPct() {
		return gmPct;
	}

	public String getPt() {
		return pt;
	}

	public String getCreated() {
		return created;
	}

	public String getBi() {
		return bi;
	}

	public String getGi() {
		return gi;
	}

	public String getOffc() {
		return offc;
	}

	public String getZa() {
		return za;
	}

	public String getMisc() {
		return misc;
	}

	public void insert(Connection conn) throws SQLException {
		
	
		if( pst == null ){
			pst = conn.prepareStatement(QueriesEnum.INSERT_DELIVERED_LINE.getQuery());
		}
		
		pst.setString(1, getPlant());		 
		pst.setString(2, getCustomerPONumber());			
		pst.setString(3, getDeliveryNumber());	
		pst.setString(4, getDelvln());		
				
		pst.addBatch();
	}
	
	public void executeBatch() throws SQLException {
		pst.executeBatch();
		pst.clearBatch();
	}
}
