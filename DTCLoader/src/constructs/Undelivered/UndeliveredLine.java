package constructs.Undelivered;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import utilities.QueriesEnum;
import utilities.UtilDelimiterParser;

public class UndeliveredLine {

	private PreparedStatement pst;
	private UtilDelimiterParser dParse;
	private String profitCenter;
	private String prodHDesc;
	private String salesOrder;
	private String cr;
	private String bl;
	private String ov;
	private String gb;
	private String customerId;
	private String customerName;
	private String customerPoNumber;
	private String requestDate;
	private String cancelDate;
	private String lineNumber;
	private String matl;
	private String cat;
	private String plant;
	private String loc;
	private String batch;
	private String itemNumber;
	private String orderQty;
	private String openQty;
	private String confQty;
	private String futureQty;
	private String lastDate;
	private String netPrice;
	private String fillQty;
	private String fillPct;
	private String extPrice;
	private String fillDollars;
	private String unitCost;
	private String extCost;
	private String gmDollars;
	private String gmPct;
	private String dc;
	private String mark;
	private String created;
	private String inHouse;
	private String reqCan;
	private String csr;
	private String futrFill;
	private String futrPct;
	private String rj;
	private String ds;
	private String os;
	private String lf;
	private String inc1;
	private String inc2;
	private String srce;
	private String cd;
	private String pp;
	private String offc;
	private String za01;
	private String x;
	
	public UndeliveredLine( String d) {
		dParse = new UtilDelimiterParser(d);
	}
	
	public void load(String data) {
			
		dParse.loadDataLine(data);
		
		profitCenter = dParse.getNextWidget();
		prodHDesc = dParse.getNextWidget();
		salesOrder = dParse.getNextWidget();
		cr = dParse.getNextWidget();
		bl = dParse.getNextWidget();
		ov = dParse.getNextWidget();
		gb = dParse.getNextWidget();
		customerId = dParse.getNextWidget();
		customerName = dParse.getNextWidget();
		customerPoNumber = dParse.getNextWidget();
		requestDate = dParse.getNextWidget();
		cancelDate = dParse.getNextWidget();
		lineNumber = dParse.getNextWidget();
		matl = dParse.getNextWidget();
		cat = dParse.getNextWidget();
		plant = dParse.getNextWidget();
		loc = dParse.getNextWidget();
		batch = dParse.getNextWidget();
		itemNumber = dParse.getNextWidget();
		orderQty = dParse.getNextWidget();
		openQty = dParse.getNextWidget();
		confQty = dParse.getNextWidget();
		futureQty = dParse.getNextWidget();
		lastDate = dParse.getNextWidget();
		netPrice = dParse.getNextWidget();
		fillQty = dParse.getNextWidget();
		fillPct = dParse.getNextWidget();
		extPrice = dParse.getNextWidget();
		fillDollars = dParse.getNextWidget();
		unitCost = dParse.getNextWidget();
		extCost = dParse.getNextWidget();
		gmDollars = dParse.getNextWidget();
		gmPct = dParse.getNextWidget();
		dc = dParse.getNextWidget();
		mark = dParse.getNextWidget();
		created = dParse.getNextWidget();
		inHouse = dParse.getNextWidget();
		reqCan = dParse.getNextWidget();
		csr = dParse.getNextWidget();
		futrFill = dParse.getNextWidget();
		futrPct = dParse.getNextWidget();
		rj = dParse.getNextWidget();
		ds = dParse.getNextWidget();
		os = dParse.getNextWidget();
		lf = dParse.getNextWidget();
		inc1 = dParse.getNextWidget();
		inc2 = dParse.getNextWidget();
		srce = dParse.getNextWidget();
		cd = dParse.getNextWidget();
		pp = dParse.getNextWidget();
		offc = dParse.getNextWidget();
		za01 = dParse.getNextWidget();
		x = dParse.getNextWidget();
	}
	
	public void reset() {
		
		profitCenter = null;
		prodHDesc = null;
		salesOrder = null;
		cr = null;
		bl = null;
		ov = null;
		gb = null;
		customerId = null;
		customerName = null;
		customerPoNumber = null;
		requestDate = null;
		cancelDate = null;
		lineNumber = null;
		matl = null;
		cat = null;
		plant = null;
		loc = null;
		batch = null;
		itemNumber = null;
		orderQty = null;
		openQty = null;
		confQty = null;
		futureQty = null;
		lastDate = null;
		netPrice = null;
		fillQty = null;
		fillPct = null;
		extPrice = null;
		fillDollars = null;
		unitCost = null;
		extCost = null;
		gmDollars = null;
		gmPct = null;
		dc = null;
		mark = null;
		created = null;
		inHouse = null;
		reqCan = null;
		csr = null;
		futrFill = null;
		futrPct = null;
		rj = null;
		ds = null;
		os = null;
		lf = null;
		inc1 = null;
		inc2 = null;
		srce = null;
		cd = null;
		pp = null;
		offc = null;
		za01 = null;
		x = null;
	}
	
	public String getProfitCenter() {
		return profitCenter;
	}

	public String getProdHDesc() {
		return prodHDesc;
	}

	public String getSalesOrder() {
		return salesOrder;
	}

	public String getCr() {
		return cr;
	}

	public String getBl() {
		return bl;
	}

	public String getOv() {
		return ov;
	}

	public String getGb() {
		return gb;
	}

	public String getCustomerId() {
		return customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getCustomerPoNumber() {
		return customerPoNumber;
	}

	public String getRequestDate() {
		return requestDate;
	}

	public String getCancelDate() {
		return cancelDate;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public String getMatl() {
		return matl;
	}

	public String getCat() {
		return cat;
	}

	public String getPlant() {
		return plant;
	}

	public String getLoc() {
		return loc;
	}

	public String getBatch() {
		return batch;
	}

	public String getItemNumber() {
		return itemNumber;
	}

	public String getOrderQty() {
		return orderQty;
	}

	public String getOpenQty() {
		return openQty;
	}

	public int getOpenQtyIntVal() {
		
		int iVal = 0;
		try {
			iVal = Integer.parseInt(openQty);
		}
		catch(Exception ex ) { 
			iVal = 0; 
		}
		
		return iVal;
	}
	
	public String getConfQty() {
		return confQty;
	}

	public String getFutureQty() {
		return futureQty;
	}

	public String getLastDate() {
		return lastDate;
	}

	public String getNetPrice() {
		return netPrice;
	}

	public String getFillQty() {
		return fillQty;
	}

	public String getFillPct() {
		return fillPct;
	}



	public String getExtPrice() {
		return extPrice;
	}

	public String getFillDollars() {
		return fillDollars;
	}

	public String getUnitCost() {
		return unitCost;
	}

	public String getExtCost() {
		return extCost;
	}

	public String getGmDollars() {
		return gmDollars;
	}

	public String getGmPct() {
		return gmPct;
	}

	public String getDc() {
		return dc;
	}

	public String getMark() {
		return mark;
	}

	public String getCreated() {
		return created;
	}

	public String getInHouse() {
		return inHouse;
	}

	public String getReqCan() {
		return reqCan;
	}

	public String getCsr() {
		return csr;
	}

	public String getFutrFill() {
		return futrFill;
	}

	public String getFutrPct() {
		return futrPct;
	}

	public String getRj() {
		return rj;
	}

	public String getDs() {
		return ds;
	}

	public String getOs() {
		return os;
	}

	public String getLf() {
		return lf;
	}

	public String getInc1() {
		return inc1;
	}

	public String getInc2() {
		return inc2;
	}

	public String getSrce() {
		return srce;
	}

	public String getCd() {
		return cd;
	}

	public String getPp() {
		return pp;
	}

	public String getOffc() {
		return offc;
	}

	public String getZa01() {
		return za01;
	}

	public String getX() {
		return x;
	}

	public void insert(Connection conn) throws SQLException {
		
		if( pst == null ){
			pst = conn.prepareStatement(QueriesEnum.INSERT_UNDELIVERED_LINE.getQuery());
		}
		
		pst.setString(1, getSalesOrder());
		pst.setString(2, getCustomerId());
		pst.setString(3, getPlant());
		pst.setString(4, getOpenQty());
		pst.setString(5, getNetPrice());
		pst.setString(6, getCreated());
		pst.setString(7, getRequestDate());
                pst.setString(8, getCr());
                pst.setString(9, getBl());
		
		pst.addBatch();
	}
	
	public void executeBatch() throws SQLException {
		pst.executeBatch();
		pst.clearBatch();
	}
}
