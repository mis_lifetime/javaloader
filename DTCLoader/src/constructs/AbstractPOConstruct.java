package constructs;

import java.util.HashMap;
import java.util.Iterator;

import utilities.ErrorEnum;
import utilities.UtilDelimiterParser;

public abstract class AbstractPOConstruct implements Insertable {

	protected HashMap<String, String> dtcTableMap;
	protected long processId;
	protected String jobId;
	protected String fileName;
	
	protected String billToId;
	protected String customerPoNumber;
	protected String plant;
	protected String insertSql;
	protected UtilDelimiterParser dParse;
	
	public AbstractPOConstruct(long pid, String jid, String fName, String dataLine, String delimiter) {
		dParse = new UtilDelimiterParser(dataLine, delimiter);
		processId = pid;
		jobId = jid;
		fileName = fName;
	}
	
	public AbstractPOConstruct(String cpoNum, String btid, String p) {
		customerPoNumber = cpoNum;
		billToId = btid;
		plant = p;
	}
	
	public FileLogEntry getFileLogEntry() {
		return new FileLogEntry(processId, jobId, "DTCINS", fileName, getBillToId(), getCustomerPoNumber(), getPlant());
	}
	
	public ErrorLogEntry getErrorLogEntry(ErrorEnum errCode, String msg) {
		return new ErrorLogEntry(processId, jobId, fileName, billToId, getCustomerPoNumber(), "DTCINS", 
				errCode, msg);
	}
	
	public String getBillToId() {
		return billToId;
	}
	
	public String getCustomerPoNumber() {
		return customerPoNumber;
	}
	
	public String getPlant() {
		return plant;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public HashMap<String, String> getDtcTableMap() {
		return dtcTableMap;
	}
	
	protected abstract void loadDtcTableMap();
	
	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
	    
		Iterator<String> i = dtcTableMap.keySet().iterator();
		while( i.hasNext() ) {
			String key = i.next();
			str.append(key + ": " + dtcTableMap.get(key) + "\n");
		}
		
		return str.toString();
	}
}
