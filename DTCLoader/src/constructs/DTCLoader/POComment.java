package constructs.DTCLoader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import constructs.AbstractPOConstruct;

import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;



public class POComment extends AbstractPOConstruct {
		
	private int sequenceNumber;
	private String lineNumber;
	private String type;
	private String msgText1;
	private String msgText2;
	private String msgText3;
	private String msgText4;
	private String msgText5;
	private String msgText6;
	private String msgText7;
	private Date modifiedDate;
	private String pListMsg;
	private String custField1;
	private String custField2;
	private String custField3;
	private String custField4;
	private String custField5;
	
	public POComment(long pid, String jid, String fname, String dataLine, String delimiter) {
		
		super(pid, jid, fname, dataLine, delimiter);		
		
		billToId = dParse.getNextWidget();
		customerPoNumber = dParse.getNextWidget();
		lineNumber = dParse.getNextWidget();		
		setSequenceNumber(dParse.getNextWidget());
		plant = dParse.getNextWidget();
		type = dParse.getNextWidget();
		msgText1 = dParse.getNextWidget();
		msgText2 = dParse.getNextWidget();
		msgText3 = dParse.getNextWidget();
		msgText4 = dParse.getNextWidget();
		msgText5 = dParse.getNextWidget();
		msgText6 = dParse.getNextWidget();
		msgText7 = dParse.getNextWidget();
		modifiedDate = DaUtilities.getDateFromString(dParse.getNextWidget());		
		custField1 = dParse.getNextWidget();
		custField2 = dParse.getNextWidget();
		custField3 = dParse.getNextWidget();
		custField4 = dParse.getNextWidget();
		custField5 = dParse.getNextWidget();
		pListMsg = dParse.getNextWidget();
		
		loadDtcTableMap();
	}
	
	public POComment(String btid, String cponum, String pid) {
		super(btid, cponum, pid);
	}
	
	public void loadFromResultSet(ResultSet rs) throws SQLException {
					
		sequenceNumber = rs.getInt("sequencenumber");
		lineNumber = rs.getString("linenumber");
		type = rs.getString("type");
		msgText1 = rs.getString("msgtext1");
		msgText2 = rs.getString("msgtext2");
		msgText3 = rs.getString("msgtext3");
		msgText4 = rs.getString("msgtext4");
		msgText5 = rs.getString("msgtext5");
		msgText6 = rs.getString("msgtext6");
		msgText7 = rs.getString("msgtext7");
		modifiedDate = (rs.getDate("modifiedDate")==null) ? null : new Date(rs.getDate("modifiedDate").getTime());
		pListMsg = rs.getString("plistmsg");
		custField1 = rs.getString("custfield1");
		custField2 = rs.getString("custfield2");
		custField3 = rs.getString("custfield3");
		custField4 = rs.getString("custfield4");
		custField5 = rs.getString("custfield5");						
					
		loadDtcTableMap();
	}
	
	protected void loadDtcTableMap() {
		dtcTableMap = new HashMap<String, String>();
		dtcTableMap.put("billToId", billToId);
		dtcTableMap.put("customerPoNumber", customerPoNumber);
		dtcTableMap.put("plant",plant);
		dtcTableMap.put("sequenceNumber", new Integer(sequenceNumber).toString());
		dtcTableMap.put("lineNumber", lineNumber);
		dtcTableMap.put("type", type);
		dtcTableMap.put("msgText1", msgText1);
		dtcTableMap.put("msgText2", msgText2);
		dtcTableMap.put("msgText3", msgText3);
		dtcTableMap.put("msgText4", msgText4);
		dtcTableMap.put("msgText5", msgText5);
		dtcTableMap.put("msgText6", msgText6);
		dtcTableMap.put("msgText7", msgText7);
		dtcTableMap.put("modifiedDate", (modifiedDate==null) ? null : modifiedDate.toString());
		dtcTableMap.put("pListMsg", pListMsg);
		dtcTableMap.put("custField1", custField1);
		dtcTableMap.put("custField2", custField2);
		dtcTableMap.put("custField3", custField3);
		dtcTableMap.put("custField4", custField4);
		dtcTableMap.put("custField5", custField5);
	}
		
	private void setSequenceNumber(String s) {
		try {
			sequenceNumber = Integer.parseInt(s);
		}
		catch(Exception ex) {
			sequenceNumber = 0;
		}
	}
	
	public String getBillToId() {
		return billToId;
	}

	public String getCustomerPoNumber() {
		return customerPoNumber;
	}

	public String getPlant() {
		return plant;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public String getType() {
		return type;
	}

	public String getMsgText1() {
		return msgText1;
	}

	public String getMsgText2() {
		return msgText2;
	}

	public String getMsgText3() {
		return msgText3;
	}

	public String getMsgText4() {
		return msgText4;
	}

	public String getMsgText5() {
		return msgText5;
	}

	public String getMsgText6() {
		return msgText6;
	}

	public String getMsgText7() {
		return msgText7;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public String getPListMsg() {
		return pListMsg;
	}

	public String getCustField1() {
		return custField1;
	}

	public String getCustField2() {
		return custField2;
	}

	public String getCustField3() {
		return custField3;
	}

	public String getCustField4() {
		return custField4;
	}

	public String getCustField5() {
		return custField5;
	}	
	
	public boolean insert(Connection conn) {
		
		UtilPreparedStatement ups = new UtilPreparedStatement(QueriesEnum.INSERT_PO_COMMENT.getQuery());
		ups.setString(1, getBillToId());
		ups.setString(2, getCustomerPoNumber());
		ups.setString(3, getPlant());
		ups.setInt(4, getSequenceNumber());
		ups.setString(5, getLineNumber());
		ups.setString(6, getType());
		ups.setString(7, getMsgText1());
		ups.setString(8, getMsgText2());
		ups.setString(9, getMsgText3());
		ups.setString(10, getMsgText4());
		ups.setString(11, getMsgText5());
		ups.setString(12, getMsgText6());
		ups.setString(13, getMsgText7());
		ups.setDate(14, getModifiedDate());
		ups.setString(15, getPListMsg());
		ups.setString(16, getCustField1());
		ups.setString(17, getCustField2());
		ups.setString(18, getCustField3());
		ups.setString(19, getCustField4());
		ups.setString(20, getCustField5());
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}
}
