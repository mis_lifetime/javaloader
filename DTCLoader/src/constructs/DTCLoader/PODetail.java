package constructs.DTCLoader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import constructs.AbstractPOConstruct;

import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilDelimiterParser;
import utilities.UtilPreparedStatement;

public class PODetail extends AbstractPOConstruct {
	
	private String lineNumber;
	private String poLineNumber;	
	private int qtyOrdered;
	private float unitPrice;
	private String upcCode;
	private String customerSellPrice;
	private String itemDescription;
	private Date modifiedDate;
	private String sku;
	private String comp;
	private String discountRate;
	private String giftNumber;
	private String giftSequenceNumber;
	private String giftWrap;
	private String showPrice;
	private String giftWrapCharge;
	private String prop65;
	private String colorCode;
	private String sizeCode;
	private String itemTaxType;
	private String receiptId;
	private String raNumber;
	private String customerPartNumber;
	private String itemTotalPrice;
	private String encodedPrice;
	private String custField1;
	private String custField2;
	private String custField3;
	private String custField4;
	private String custField5;
	private String custField6;
	private String custField7;
	private String custField8;
	private String custField9;
	private String custField10;
	private String custField11;
	private String custField12;
	private String custField13;
	private String custField14;
	private String custField15;
	private String custField16;
	private String custField17;
	private String custField18;
	private String custField19;
	private String custField20;
	
	public PODetail(long pid, String jid, String fname, String dataLine, String delimiter) {
		
		super(pid, jid, fname, dataLine, delimiter);
		processId = pid;
		fileName = fname;
		
		UtilDelimiterParser dParse = new UtilDelimiterParser(dataLine, delimiter);
		
		billToId = dParse.getNextWidget();
		customerPoNumber = dParse.getNextWidget();
		lineNumber = dParse.getNextWidget();		
		plant = dParse.getNextWidget();				
		poLineNumber = dParse.getNextWidget();
		setQtyOrdered(dParse.getNextWidget());
		setUnitPrice(dParse.getNextWidget());
		upcCode = dParse.getNextWidget();
		customerSellPrice = dParse.getNextWidget();
		itemDescription = dParse.getNextWidget();
		modifiedDate = DaUtilities.getDateFromString(dParse.getNextWidget());
		setSku(dParse.getNextWidget());
		setComp(dParse.getNextWidget());
		discountRate = dParse.getNextWidget();
		giftNumber = dParse.getNextWidget();
		giftSequenceNumber = dParse.getNextWidget();
		giftWrap = dParse.getNextWidget();
		showPrice = dParse.getNextWidget();
		giftWrapCharge = dParse.getNextWidget();
		prop65 = dParse.getNextWidget();
		colorCode = dParse.getNextWidget();
		custField1 = dParse.getNextWidget();
		custField10 = dParse.getNextWidget();
		custField11 = dParse.getNextWidget();
		custField12 = dParse.getNextWidget();
		custField13 = dParse.getNextWidget();
		custField14 = dParse.getNextWidget();
		custField15 = dParse.getNextWidget();
		custField16 = dParse.getNextWidget();
		custField17 = dParse.getNextWidget();
		custField18 = dParse.getNextWidget();
		custField19 = dParse.getNextWidget();
		custField2 = dParse.getNextWidget();
		custField20 = dParse.getNextWidget();
		custField3 = dParse.getNextWidget();
		custField4 = dParse.getNextWidget();
		custField5 = dParse.getNextWidget();
		custField6 = dParse.getNextWidget();
		custField7 = dParse.getNextWidget();
		custField8 = dParse.getNextWidget();
		custField9 = dParse.getNextWidget();
		customerPartNumber = dParse.getNextWidget();
		encodedPrice = dParse.getNextWidget();
		itemTaxType = dParse.getNextWidget();
		itemTotalPrice = dParse.getNextWidget();
		raNumber = dParse.getNextWidget();
		receiptId = dParse.getNextWidget();
		sizeCode = dParse.getNextWidget();																					
		
		loadDtcTableMap();
	}
	
	public PODetail(String btid, String cponum, String plant) {
		super(btid, cponum, plant);		
	}
	
	public void loadFromResultSet(ResultSet rs) throws SQLException {
				
		comp = rs.getString("comp");				
		lineNumber = rs.getString("linenumber");		
		poLineNumber = rs.getString("polinenumber");		
		qtyOrdered = rs.getInt("qtyordered");
		unitPrice = rs.getFloat("unitprice");
		upcCode = rs.getString("upccode");
		customerSellPrice = rs.getString("customersellprice");
		itemDescription = rs.getString("itemdescription");
		modifiedDate = (rs.getDate("modifieddate")==null) ? null : new Date(rs.getDate("modifieddate").getTime());		
		discountRate = rs.getString("discountrate");
		giftNumber = rs.getString("giftnumber");
		giftSequenceNumber = rs.getString("giftseqnumber");
		giftWrap = rs.getString("giftwrap");
		showPrice = rs.getString("showprice");
		giftWrapCharge = rs.getString("giftwrapcharge");
		prop65 = rs.getString("prop65");
		colorCode = rs.getString("colorcode");
		sizeCode = rs.getString("sizecode");
		itemTaxType = rs.getString("itemtaxtype");
		receiptId = rs.getString("receiptid");
		raNumber = rs.getString("ranumber");
		customerPartNumber = rs.getString("customerpartnumber");
		itemTotalPrice = rs.getString("itemtotalprice");
		encodedPrice = rs.getString("encodedprice");
		custField1 = rs.getString("custfield1");
		custField2 = rs.getString("custfield2");
		custField3 = rs.getString("custfield3");
		custField4 = rs.getString("custfield4");
		custField5 = rs.getString("custfield5");
		custField6 = rs.getString("custfield6");
		custField7 = rs.getString("custfield7");
		custField8 = rs.getString("custfield8");
		custField9 = rs.getString("custfield9");
		custField10 = rs.getString("custfield10");
		custField11 = rs.getString("custfield11");
		custField12 = rs.getString("custfield12");
		custField13 = rs.getString("custfield13");
		custField14 = rs.getString("custfield14");
		custField15 = rs.getString("custfield15");
		custField16 = rs.getString("custfield16");
		custField17 = rs.getString("custfield17");
		custField18 = rs.getString("custfield18");
		custField19 = rs.getString("custfield19");
		custField20 = rs.getString("custfield20");
		
		loadDtcTableMap();
	}
	
	protected void loadDtcTableMap() {
		dtcTableMap = new HashMap<String, String>();
		dtcTableMap.put("billToId", billToId);
		dtcTableMap.put("customerPoNumber", customerPoNumber);
		dtcTableMap.put("plant",plant);
		dtcTableMap.put("poLineNumber", poLineNumber);
		dtcTableMap.put("lineNumber", lineNumber);
		dtcTableMap.put("qtyOrdered", new Integer(qtyOrdered).toString());
		dtcTableMap.put("unitPrice",new Float(unitPrice).toString());
		dtcTableMap.put("upcCode", upcCode);
		dtcTableMap.put("customerSellPrice", customerSellPrice);
		dtcTableMap.put("itemDescription", itemDescription);
		dtcTableMap.put("modifiedDate", (modifiedDate==null) ? null : modifiedDate.toString());
		dtcTableMap.put("sku", sku);
		dtcTableMap.put("comp", comp);
		dtcTableMap.put("discountRate", discountRate);
		dtcTableMap.put("giftNumber", giftNumber);
		dtcTableMap.put("giftSequenceNumber", giftSequenceNumber);
		dtcTableMap.put("giftWrap", giftWrap);
		dtcTableMap.put("showPrice", showPrice);
		dtcTableMap.put("giftWrapCharge", giftWrapCharge);
		dtcTableMap.put("prop65", prop65);
		dtcTableMap.put("colorCode", colorCode);
		dtcTableMap.put("sizeCode", sizeCode);
		dtcTableMap.put("itemTaxType", itemTaxType);
		dtcTableMap.put("receiptId", receiptId);
		dtcTableMap.put("raNumber", raNumber);
		dtcTableMap.put("customerPartNumber", customerPartNumber);
		dtcTableMap.put("itemTotalPrice", itemTotalPrice);
		dtcTableMap.put("encodedPrice", encodedPrice);
		dtcTableMap.put("custField1", custField1);
		dtcTableMap.put("custField2", custField2);
		dtcTableMap.put("custField3", custField3);
		dtcTableMap.put("custField4", custField4);
		dtcTableMap.put("custField5", custField5);
		dtcTableMap.put("custField6", custField6);
		dtcTableMap.put("custField7", custField7);
		dtcTableMap.put("custField8", custField8);
		dtcTableMap.put("custField9", custField9);
		dtcTableMap.put("custField10", custField10);
		dtcTableMap.put("custField11", custField11);
		dtcTableMap.put("custField12", custField12);
		dtcTableMap.put("custField13", custField13);
		dtcTableMap.put("custField14", custField14);
		dtcTableMap.put("custField15", custField15);
		dtcTableMap.put("custField16", custField16);
		dtcTableMap.put("custField17", custField17);
		dtcTableMap.put("custField18", custField18);
		dtcTableMap.put("custField18", custField19);
		dtcTableMap.put("custField20", custField20);
	}

	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	public void setPoLineNumber(String poLineNumber) {
		this.poLineNumber = poLineNumber;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}

	public void setCustomerSellPrice(String customerSellPrice) {
		this.customerSellPrice = customerSellPrice;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void setDiscountRate(String discountRate) {
		this.discountRate = discountRate;
	}

	public void setGiftNumber(String giftNumber) {
		this.giftNumber = giftNumber;
	}

	public void setGiftSequenceNumber(String giftSequenceNumber) {
		this.giftSequenceNumber = giftSequenceNumber;
	}

	public void setGiftWrap(String giftWrap) {
		this.giftWrap = giftWrap;
	}

	public void setShowPrice(String showPrice) {
		this.showPrice = showPrice;
	}

	public void setGiftWrapCharge(String giftWrapCharge) {
		this.giftWrapCharge = giftWrapCharge;
	}

	public void setProp65(String prop65) {
		this.prop65 = prop65;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	public void setSizeCode(String sizeCode) {
		this.sizeCode = sizeCode;
	}

	public void setItemTaxType(String itemTaxType) {
		this.itemTaxType = itemTaxType;
	}

	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public void setRaNumber(String raNumber) {
		this.raNumber = raNumber;
	}

	public void setCustomerPartNumber(String customerPartNumber) {
		this.customerPartNumber = customerPartNumber;
	}

	public void setItemTotalPrice(String itemTotalPrice) {
		this.itemTotalPrice = itemTotalPrice;
	}

	public void setEncodedPrice(String encodedPrice) {
		this.encodedPrice = encodedPrice;
	}

	public void setCustField1(String custField1) {
		this.custField1 = custField1;
	}

	public void setCustField2(String custField2) {
		this.custField2 = custField2;
	}

	public void setCustField3(String custField3) {
		this.custField3 = custField3;
	}

	public void setCustField4(String custField4) {
		this.custField4 = custField4;
	}

	public void setCustField5(String custField5) {
		this.custField5 = custField5;
	}

	public void setCustField6(String custField6) {
		this.custField6 = custField6;
	}

	public void setCustField7(String custField7) {
		this.custField7 = custField7;
	}

	public void setCustField8(String custField8) {
		this.custField8 = custField8;
	}

	public void setCustField9(String custField9) {
		this.custField9 = custField9;
	}

	public void setCustField10(String custField10) {
		this.custField10 = custField10;
	}

	public void setCustField11(String custField11) {
		this.custField11 = custField11;
	}

	public void setCustField12(String custField12) {
		this.custField12 = custField12;
	}

	public void setCustField13(String custField13) {
		this.custField13 = custField13;
	}

	public void setCustField14(String custField14) {
		this.custField14 = custField14;
	}

	public void setCustField15(String custField15) {
		this.custField15 = custField15;
	}

	public void setCustField16(String custField16) {
		this.custField16 = custField16;
	}

	public void setCustField17(String custField17) {
		this.custField17 = custField17;
	}

	public void setCustField18(String custField18) {
		this.custField18 = custField18;
	}

	public void setCustField19(String custField19) {
		this.custField19 = custField19;
	}

	public void setCustField20(String custField20) {
		this.custField20 = custField20;
	}

	private void setSku(String s) {
		sku = DaUtilities.checkForNullInNotNullableField(s);
	}
	
	private void setQtyOrdered(String s) {
		qtyOrdered = DaUtilities.parseStringToInt(s);
	}
	
	private void setComp(String s) {
		comp = DaUtilities.checkForNullInNotNullableField(s);
	}
	
	private void setUnitPrice(String s) {
		unitPrice = DaUtilities.translateTextToCurrency(s);
	}
	
	public String getBillToId() {
		return billToId;
	}

	public String getCustomerPoNumber() {
		return customerPoNumber;
	}

	public String getPlant() {
		return plant;
	}

	public String getPoLineNumber() {
		return poLineNumber;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public String getUpcCode() {
		return upcCode;
	}

	public String getCustomerSellPrice() {
		return customerSellPrice;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public String getSku() {
		return sku;
	}

	public String getComp() {
		return comp;
	}

	public String getDiscountRate() {
		return discountRate;
	}

	public String getGiftNumber() {
		return giftNumber;
	}

	public String getGiftSequenceNumber() {
		return giftSequenceNumber;
	}

	public String getGiftWrap() {
		return giftWrap;
	}

	public String getShowPrice() {
		return showPrice;
	}

	public String getGiftWrapCharge() {
		return giftWrapCharge;
	}

	public String getProp65() {
		return prop65;
	}

	public String getColorCode() {
		return colorCode;
	}

	public String getSizeCode() {
		return sizeCode;
	}

	public String getItemTaxType() {
		return itemTaxType;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public String getRaNumber() {
		return raNumber;
	}

	public String getCustomerPartNumber() {
		return customerPartNumber;
	}

	public String getItemTotalPrice() {
		return itemTotalPrice;
	}

	public String getEncodedPrice() {
		return encodedPrice;
	}

	public String getCustField1() {
		return custField1;
	}

	public String getCustField2() {
		return custField2;
	}

	public String getCustField3() {
		return custField3;
	}

	public String getCustField4() {
		return custField4;
	}

	public String getCustField5() {
		return custField5;
	}

	public String getCustField6() {
		return custField6;
	}

	public String getCustField7() {
		return custField7;
	}

	public String getCustField8() {
		return custField8;
	}

	public String getCustField9() {
		return custField9;
	}

	public String getCustField10() {
		return custField10;
	}

	public String getCustField11() {
		return custField11;
	}

	public String getCustField12() {
		return custField12;
	}

	public String getCustField13() {
		return custField13;
	}

	public String getCustField14() {
		return custField14;
	}

	public String getCustField15() {
		return custField15;
	}

	public String getCustField16() {
		return custField16;
	}

	public String getCustField17() {
		return custField17;
	}

	public String getCustField18() {
		return custField18;
	}

	public String getCustField19() {
		return custField19;
	}

	public String getCustField20() {
		return custField20;
	}	
	
	public boolean insert(Connection conn) {		
		
		UtilPreparedStatement ups = new UtilPreparedStatement(QueriesEnum.INSERT_PO_DETAIL_2.getQuery());
		ups.setString(1, billToId);
		ups.setString(2, customerPoNumber);
		ups.setString(3, poLineNumber);
		ups.setString(4, plant);
		ups.setString(5, lineNumber);
		ups.setInt(6, qtyOrdered);
		ups.setFloat(7,unitPrice);
		ups.setString(8,upcCode);
		ups.setString(9,customerSellPrice);
		ups.setString(10, itemDescription);
		ups.setDate(11, modifiedDate);
		ups.setString(12, sku);
		ups.setString(13, comp);
		ups.setString(14, discountRate);
		ups.setString(15, giftNumber);
		ups.setString(16, giftSequenceNumber);
		ups.setString(17, giftWrap);
		ups.setString(18, showPrice);
		ups.setString(19, giftWrapCharge);
		ups.setString(20, prop65);
		ups.setString(21, colorCode);
		ups.setString(22, sizeCode);				
		ups.setString(23, itemTaxType);
		ups.setString(24, receiptId);
		ups.setString(25, raNumber);
		ups.setString(26, customerPartNumber);
		ups.setString(27, itemTotalPrice);
		ups.setString(28, encodedPrice);
		ups.setString(29, custField1);
		ups.setString(30, custField2);
		ups.setString(31, custField3);
		ups.setString(32, custField4);
		ups.setString(33, custField5);
		ups.setString(34, custField6);
		ups.setString(35, custField7);
		ups.setString(36, custField8);
		ups.setString(37, custField9);
		ups.setString(38, custField10);
		ups.setString(39, custField11);
		ups.setString(40, custField12);
		ups.setString(41, custField13);
		ups.setString(42, custField14);
		ups.setString(43, custField15);
		ups.setString(44, custField16);
		ups.setString(45, custField17);
		ups.setString(46, custField18);
		ups.setString(47, custField19);
		ups.setString(48, custField20);
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}
}
