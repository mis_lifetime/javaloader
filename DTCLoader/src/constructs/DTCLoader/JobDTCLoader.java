package constructs.DTCLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

import utilities.DaUtilities;
import utilities.ErrorEnum;
import constructs.AbstractPOConstruct;
import constructs.EMJob;
import constructs.ProcessLogEntry;

public class JobDTCLoader extends EMJob {

	private Connection dtcConnection;
	private static final String PO_HEADER_DELIMITER = "|";
	private static String LOAD_FROM_DIR;
	private static String COPY_TO_DIR;
	private static String LOG_FILE_DIR;
	private static long procId;
	//private FileOutputStream fos = null;
	//private PrintStream ps = null;
	
	
	public JobDTCLoader() throws FileNotFoundException,
			IOException {
		super("resources/DTCLoaderProperties");					
	}

	@Override
	public void postProcessEMJob() throws SQLException, IOException {
		
		DaUtilities.writeMessage("Closing files and connections", false);
		dtcConnection.close();
		dtcConnection = null;	
		//fos.close();
		//ps.close();
		//fos = null;
		//ps = null;
		System.gc();
	}

	@Override
	public void preProcessEMJob() throws IOException, SQLException, ClassNotFoundException {
		procId = Calendar.getInstance().getTimeInMillis();	
		LOAD_FROM_DIR = jobProperties.getProperty("INPUTDIR");
		COPY_TO_DIR = jobProperties.getProperty("OUTPUTDIR");
		LOG_FILE_DIR = jobProperties.getProperty("LOGDIR");
																			
		dtcConnection = DaUtilities.getOracleConnection(jobProperties.getProperty("CONNECTION"));
		//fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId + "_runlog.txt");
				
		//ps = new PrintStream(fos);
		//System.setOut(ps);
		//System.setErr(ps);
		
		DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);
		DaUtilities.writeMessage("COPY_TO_DIR: " + COPY_TO_DIR, false);
		DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);
		DaUtilities.writeMessage(dtcConnection.toString(), false);		
	}

	@Override
	public void processEMJob() throws FileNotFoundException, IOException{
		File dtcFolder = new File(LOAD_FROM_DIR);
		File copyFolder = new File(COPY_TO_DIR);
		File[] dtcOrderFiles = dtcFolder.listFiles();
		BufferedReader inputStream = null;
		int numFiles = 0;
		int numRows = 0;
		int numErrs = 0;

		DaUtilities.writeMessage("Starting Process Log", false);
		ProcessLogEntry ple = new ProcessLogEntry(procId, super.getJobId());
		ple.startLog(dtcConnection);
		
		DaUtilities.writeMessage("Processing " + dtcOrderFiles.length
				+ " files", false);
		for (int i = 0; i < dtcOrderFiles.length; i++) {

			if (!dtcOrderFiles[i].isFile())
				continue;

			File f = dtcOrderFiles[i];
			String origName = f.getName();

			if (f.renameTo(new File(COPY_TO_DIR, f.getName() + "_" + procId))) {
				DaUtilities.writeMessage("Processing File: " + origName, false);

				f = new File(COPY_TO_DIR, origName + "_" + procId);
				numFiles++;
				inputStream = new BufferedReader(new FileReader(copyFolder
						+ "//" + f.getName()));

				String line;
				while ((line = inputStream.readLine()) != null) {

					numRows++;
					boolean isOk = false;
					if(origName.startsWith("POHDR")) {						
						isOk = processPOConstruct( new POHeader(procId, super.getJobId(), origName, line, PO_HEADER_DELIMITER) );						
					} else if(origName.startsWith("PODTL")) {						
						isOk = processPOConstruct( new PODetail(procId, super.getJobId(), origName, line, PO_HEADER_DELIMITER) );						
					} else if(origName.startsWith("POCMT")) {						
						isOk = processPOConstruct( new POComment(procId, super.getJobId(), origName, line, PO_HEADER_DELIMITER) );						
					} else{
						DaUtilities.writeMessage(origName + ": is UNKNOWN . . . ", true);
					}
					
					numErrs += (isOk) ? 0 : 1;
				}

				DaUtilities.writeMessage("Closing file input stream", false);
				inputStream.close();

				DaUtilities.writeMessage("Renaming file back to orignal name",
						false);
				if (!f.renameTo(new File(COPY_TO_DIR, origName))) {
					DaUtilities.writeMessage(
							"Could not copy file back to original name",
							true);
				}
			} else {
				DaUtilities.writeMessage("Failed to Copy File", true);
			}
		}

		DaUtilities.writeMessage("Completing process log.", false);
		ple.setNumFiles(numFiles);
		ple.setNumRows(numRows);
		ple.setEndDate();
		ple.stopLog(dtcConnection);
	}
	
	private boolean processPOConstruct(AbstractPOConstruct apoc) {
		DaUtilities.writeMessage("Processing " + apoc.getFileName() + " entry", false);
		apoc.getFileLogEntry().insert(dtcConnection);
		boolean result = apoc.insert(dtcConnection);
		if(!result) {
			apoc.getErrorLogEntry(ErrorEnum.INSERT_OCOMMENT, "Failed insert, check log").insert(dtcConnection);			
		}
		
		return result;		
	}
}
