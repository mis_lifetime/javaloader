package constructs.DTCLoader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import constructs.AbstractPOConstruct;

import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

public class POHeader extends AbstractPOConstruct {		

	private Date salesOrderDate;
	private String shipToName;
	private String shipToAddress1;
	private String shipToAddress2;
	private String shipToAddress3;
	private String shipToCity;
	private String shipToState;
	private String shipToZipCode;
	private String shipToCountry;
	private String shipToPhoneNumber;
	private String shipToEmail;
	private String poType;
	private String amazonCustomerOrderNumber;
	private String billToName;
	private String billToAddress1;
	private String billToAddress2;
	private String billToAddress3;
	private String billToCity;
	private String billToState;
	private String billToZipCode;
	private String billToCountry;
	private String billToPhoneNumber;
	private String billToEmail;
	private String marketingInsert;
	private String promoCode;
	private String promoAmount;
	private String shipFromWarehouse;
	private String carrierType;
	private String shipToAttention;
	private String giftWrap;
	private Date modifiedDate;
	private String payMethod;
	private String currency;
	private String payCardType;
	private String cardAmount;
	private String extPrice;
	private String shipCharge;
	private String taxCharge;
	private String shipTaxTotal;
	private String vendorId;
	private String markFor;
	private String shipVia;
	private String department;
	private String logo;
	private String subLogo;
	private String commitNumber;
	private String resvNumber;
	private String registNumber;
	private String batchNumber;
	private String salestax;
	private String giftWrapCharge;
	private String returnTypeCode;
	private String returnAdrCode;
	private String processYN;
	private Date processTime;
	private String giftOrder;
	private String taxChargeDesc;
	private String custServEmail;
	private String custServPhone;
	private String taxChargeB;
	private String taxChargeBDesc;
	private String taxChargeC;
	private String taxChargeCDesc;
	private String taxChargeD;
	private String taxChargeDDesc;
	private String taxChargeE;
	private String taxChargeEDesc;
	private String taxChargeF;
	private String taxChargeFDesc;
	private String orderSubTotal;
	private String orderTotal;
	private String percentOff;
	private String percentOffDesc;
	private String taxTotal;
	private String creditTotal;
	private String returnName;
	private String returnAddress1;
	private String returnAddress2;
	private String returnCity;
	private String returnState;
	private String returnZip;
	private String custField1;
	private String custField2;
	private String custField3;
	private String custField4;
	private String custField5;
	private String custField6;
	private String custField7;
	private String custField8;
	private String custField9;
	private String custField10;
	private String custField11;
	private String custField12;
	private String custField13;
	private String custField14;
	private String custField15;
	private String custField16;
	private String custField17;
	private String custField18;
	private String custField19;
	private String custField20;
	private String receiptId;
	private String pSlipVersion;
	private String buyStore;

	public POHeader(long pid, String jid, String fName, String dataLine, String delimeter) {

		super(pid, jid, fName, dataLine, delimeter);
				
		// Check how this dates coming back!!!
		billToId = dParse.getNextWidget();
		customerPoNumber = dParse.getNextWidget();
		plant = dParse.getNextWidget();
		salesOrderDate = DaUtilities.getDateFromString(dParse.getNextWidget());
		shipToName = dParse.getNextWidget();
		shipToAddress1 = dParse.getNextWidget();
		shipToAddress2 = dParse.getNextWidget();
		shipToAddress3 = dParse.getNextWidget();
		shipToCity = dParse.getNextWidget();
		shipToState = dParse.getNextWidget();
		shipToZipCode = dParse.getNextWidget();
		shipToCountry = dParse.getNextWidget();
		shipToPhoneNumber = dParse.getNextWidget();
		shipToEmail = dParse.getNextWidget();
		poType = dParse.getNextWidget();
		amazonCustomerOrderNumber = dParse.getNextWidget();
		billToName = dParse.getNextWidget();
		billToAddress1 = dParse.getNextWidget();
		billToAddress2 = dParse.getNextWidget();
		billToAddress3 = dParse.getNextWidget();
		billToCity = dParse.getNextWidget();
		billToState = dParse.getNextWidget();
		billToZipCode = dParse.getNextWidget();
		billToCountry = dParse.getNextWidget();
		billToPhoneNumber = dParse.getNextWidget();
		billToEmail = dParse.getNextWidget();
		marketingInsert = dParse.getNextWidget();
		promoCode = dParse.getNextWidget();
		promoAmount = dParse.getNextWidget();
		shipFromWarehouse = dParse.getNextWidget();
		carrierType = dParse.getNextWidget();
		shipToAttention = dParse.getNextWidget();
		giftWrap = dParse.getNextWidget();
		modifiedDate = DaUtilities.getDateFromString(dParse.getNextWidget());
		payMethod = dParse.getNextWidget();
		currency = dParse.getNextWidget();
		payCardType = dParse.getNextWidget();
		cardAmount = dParse.getNextWidget();
		extPrice = dParse.getNextWidget();
		shipCharge = dParse.getNextWidget();
		taxCharge = dParse.getNextWidget();
		shipTaxTotal = dParse.getNextWidget();
		vendorId = dParse.getNextWidget();
		markFor = dParse.getNextWidget();
		shipVia = dParse.getNextWidget();
		department = dParse.getNextWidget();
		logo = dParse.getNextWidget();
		subLogo = dParse.getNextWidget();
		commitNumber = dParse.getNextWidget();
		resvNumber = dParse.getNextWidget();
		registNumber = dParse.getNextWidget();
		batchNumber = dParse.getNextWidget();
		salestax = dParse.getNextWidget();
		giftWrapCharge = dParse.getNextWidget();
		returnTypeCode = dParse.getNextWidget();
		returnAdrCode = dParse.getNextWidget();
		processYN = dParse.getNextWidget();
		processTime = DaUtilities.getDateFromString(dParse.getNextWidget());
		buyStore = dParse.getNextWidget();		
		creditTotal = dParse.getNextWidget();
		custField1 = dParse.getNextWidget();
		custField10 = dParse.getNextWidget();
		custField11 = dParse.getNextWidget();
		custField12 = dParse.getNextWidget();
		custField13 = dParse.getNextWidget();
		custField14 = dParse.getNextWidget();
		custField15 = dParse.getNextWidget();
		custField16 = dParse.getNextWidget();
		custField17 = dParse.getNextWidget();
		custField18 = dParse.getNextWidget();
		custField19 = dParse.getNextWidget();
		custField2 = dParse.getNextWidget();
		custField20 = dParse.getNextWidget();		
		custField3 = dParse.getNextWidget();
		custField4 = dParse.getNextWidget();
		custField5 = dParse.getNextWidget();
		custField6 = dParse.getNextWidget();
		custField7 = dParse.getNextWidget();
		custField8 = dParse.getNextWidget();
		custField9 = dParse.getNextWidget();		
		custServEmail = dParse.getNextWidget();
		custServPhone = dParse.getNextWidget();
		giftOrder = dParse.getNextWidget();
		orderSubTotal = dParse.getNextWidget();
		orderTotal = dParse.getNextWidget();
		percentOff = dParse.getNextWidget();
		percentOffDesc = dParse.getNextWidget();
		pSlipVersion = dParse.getNextWidget();	
		receiptId = dParse.getNextWidget();	
		returnAddress1 = dParse.getNextWidget();
		returnAddress2 = dParse.getNextWidget();
		returnCity = dParse.getNextWidget();
		returnName = dParse.getNextWidget();
		returnState = dParse.getNextWidget();
		returnZip = dParse.getNextWidget();		
		taxChargeB = dParse.getNextWidget();
		taxChargeBDesc = dParse.getNextWidget();
		taxChargeC = dParse.getNextWidget();
		taxChargeCDesc = dParse.getNextWidget();
		taxChargeD = dParse.getNextWidget();
		taxChargeDDesc = dParse.getNextWidget();		
		taxChargeDesc = dParse.getNextWidget();			
		taxChargeE = dParse.getNextWidget();
		taxChargeEDesc = dParse.getNextWidget();
		taxChargeF = dParse.getNextWidget();
		taxChargeFDesc = dParse.getNextWidget();		
		taxTotal = dParse.getNextWidget();													
		
		loadDtcTableMap();
	}

	public POHeader(String cpoNum, String btid, String p) {
		super(cpoNum, btid, p);	
	}
	
	public void load(Connection conn, boolean loadFromProd) throws SQLException {

		UtilPreparedStatement ups = null;
		if (loadFromProd) {
			ups = new UtilPreparedStatement(QueriesEnum.SELECT_PO_HEADER
					.getQuery());
		} else {
			ups = new UtilPreparedStatement(QueriesEnum.SELECT_EM_HEADER
					.getQuery());
		}

		ups.setString(1, billToId);
		ups.setString(2, customerPoNumber);
		ups.setString(3, plant);

		Statement st = conn.createStatement();
		DaUtilities.writeMessage("POHeader executing SQL: "
				+ ups.getTranslatedSql(), false);
		ResultSet rs = st.executeQuery(ups.getTranslatedSql());
		
		if(rs.next()) {

			salesOrderDate = (rs.getDate("salesorderdate")==null) 
				? null : new Date(rs.getDate("salesorderdate").getTime());
			shipToName = rs.getString("shiptoname");
			shipToAddress1 = rs.getString("shiptoaddress1");
			shipToAddress2 = rs.getString("shiptoaddress2");
			shipToAddress3 = rs.getString("shiptoaddress3");
			shipToCity = rs.getString("shiptocity");
			shipToState = rs.getString("shiptostate");
			shipToZipCode = rs.getString("shiptozipcode");
			shipToCountry = rs.getString("shiptocountry");
			shipToPhoneNumber = rs.getString("shiptophonenum");
			shipToEmail = rs.getString("shiptoemail");
			poType = rs.getString("potype");
			amazonCustomerOrderNumber = rs
					.getString("amazoncustomerordernumber");
			billToName = rs.getString("billtoname");
			billToAddress1 = rs.getString("billtoaddress1");
			billToAddress2 = rs.getString("billtoaddress2");
			billToAddress3 = rs.getString("billtoaddress3");
			billToCity = rs.getString("billtocity");
			billToState = rs.getString("billtostate");
			billToZipCode = rs.getString("billtozipcode");
			billToCountry = rs.getString("billtocountry");
			billToPhoneNumber = rs.getString("billtophonenum");
			billToEmail = rs.getString("billtoemail");
			marketingInsert = rs.getString("marketinginsert");
			promoCode = rs.getString("promocode");
			promoAmount = rs.getString("promoamount");
			shipFromWarehouse = rs.getString("shipfromwarehouse");
			carrierType = rs.getString("carriertype");
			shipToAttention = rs.getString("shiptoattn");
			giftWrap = rs.getString("giftwrap");
			modifiedDate = (rs.getDate("modifieddate")==null) 
				? null : new Date(rs.getDate("modifieddate").getTime());
			payMethod = rs.getString("paymethod");
			currency = rs.getString("currency");
			payCardType = rs.getString("paycardtype");
			cardAmount = rs.getString("cardAmount");
			extPrice = rs.getString("extprice");
			shipCharge = rs.getString("shipcharge");
			taxCharge = rs.getString("taxcharge");
			shipTaxTotal = rs.getString("shiptaxtotal");
			vendorId = rs.getString("vendorid");
			markFor = rs.getString("markfor");
			shipVia = rs.getString("shipvia");
			department = rs.getString("department");
			logo = rs.getString("logo");
			subLogo = rs.getString("sublogo");
			commitNumber = rs.getString("sublogo");
			resvNumber = rs.getString("resvnumber");
			registNumber = rs.getString("registnumber");
			batchNumber = rs.getString("batchnumber");
			salestax = rs.getString("saletax");
			giftWrapCharge = rs.getString("giftwrapcharge");
			returnTypeCode = rs.getString("returntypecode");
			returnAdrCode = rs.getString("returnaddrcode");
			processYN = rs.getString("process_yn");
			processTime = rs.getDate("process_time");
			giftOrder = rs.getString("giftorder");
			taxChargeDesc = rs.getString("taxchargedesc");
			custServEmail = rs.getString("custservemail");
			custServPhone = rs.getString("custServPhone");
			taxChargeB = rs.getString("taxchargeb");
			taxChargeC = rs.getString("taxchargec");
			taxChargeD = rs.getString("taxcharged");
			taxChargeE = rs.getString("taxchargee");
			taxChargeF = rs.getString("taxchargef");
			taxChargeBDesc = rs.getString("taxchargebdesc");
			taxChargeCDesc = rs.getString("taxchargecdesc");
			taxChargeDDesc = rs.getString("taxchargeddesc");
			taxChargeEDesc = rs.getString("taxchargeedesc");
			taxChargeFDesc = rs.getString("taxchargefdesc");
			orderSubTotal = rs.getString("ordersubtotal");
			orderTotal = rs.getString("ordertotal");
			percentOff = rs.getString("percentoff");
			taxTotal = rs.getString("taxtotal");
			creditTotal = rs.getString("credittotal");
			returnName = rs.getString("returnname");
			returnAddress1 = rs.getString("returnaddress1");
			returnAddress2 = rs.getString("returnaddress2");
			returnCity = rs.getString("returncity");
			returnState = rs.getString("returnState");
			returnZip = rs.getString("returnzip");
			custField1 = rs.getString("custfield1");
			custField2 = rs.getString("custfield2");
			custField3 = rs.getString("custfield3");
			custField4 = rs.getString("custfield4");
			custField5 = rs.getString("custfield5");
			custField6 = rs.getString("custfield6");
			custField7 = rs.getString("custfield7");
			custField8 = rs.getString("custfield8");
			custField9 = rs.getString("custfield9");
			custField10 = rs.getString("custfield10");
			custField11 = rs.getString("custfield11");
			custField12 = rs.getString("custfield12");
			custField13 = rs.getString("custfield13");
			custField14 = rs.getString("custfield14");
			custField15 = rs.getString("custfield15");
			custField16 = rs.getString("custfield16");
			custField17 = rs.getString("custfield17");
			custField18 = rs.getString("custfield18");
			custField19 = rs.getString("custfield19");
			custField20 = rs.getString("custfield20");
			receiptId = rs.getString("receiptid");
			pSlipVersion = rs.getString("pslipversion");
			buyStore = rs.getString("buystore");
		}
		
		st.close();
		rs.close();
		st = null;
		rs = null;
		
		loadDtcTableMap();
	}

	protected void loadDtcTableMap() {
		dtcTableMap = new HashMap<String, String>();
		dtcTableMap.put("billToId", billToId);
		dtcTableMap.put("customerPoNumber", customerPoNumber);
		dtcTableMap.put("plant", plant);
		dtcTableMap.put("salesOrderDate", (salesOrderDate == null) ? null : salesOrderDate.toString());
		dtcTableMap.put("shipToName", shipToName);
		dtcTableMap.put("shipToAddress1", shipToAddress1);
		dtcTableMap.put("shipToAddress2", shipToAddress2);
		dtcTableMap.put("shipToAddress3", shipToAddress3);
		dtcTableMap.put("shipToCity", shipToCity);
		dtcTableMap.put("shipToState", shipToState);
		dtcTableMap.put("shipToZipCode", shipToZipCode);
		dtcTableMap.put("shipToCountry", shipToCountry);
		dtcTableMap.put("shipToPhoneNumber", shipToPhoneNumber);
		dtcTableMap.put("shipToEmail", shipToEmail);
		dtcTableMap.put("poType", poType);
		dtcTableMap.put("amazonCustomerOrderNumber", amazonCustomerOrderNumber);
		dtcTableMap.put("billToName", billToName);
		dtcTableMap.put("billToAddress1", billToAddress1);
		dtcTableMap.put("billToAddress2", billToAddress2);
		dtcTableMap.put("billToAddress3", billToAddress3);
		dtcTableMap.put("billToCity", billToCity);
		dtcTableMap.put("billToState", billToState);
		dtcTableMap.put("billToZipCode", billToZipCode);
		dtcTableMap.put("billToCountry", billToCountry);
		dtcTableMap.put("billToPhoneNumber", billToPhoneNumber);
		dtcTableMap.put("billToEmail", billToEmail);
		dtcTableMap.put("marketingInsert", marketingInsert);
		dtcTableMap.put("promoCode", promoCode);
		dtcTableMap.put("promoAmount", promoAmount);
		dtcTableMap.put("shipFromWarehouse", shipFromWarehouse);
		dtcTableMap.put("carrierType", carrierType);
		dtcTableMap.put("shipToAttention", shipToAttention);
		dtcTableMap.put("giftWrap", giftWrap);
		dtcTableMap.put("modifiedDate", (modifiedDate == null) ? null : modifiedDate.toString());
		dtcTableMap.put("payMethod", payMethod);
		dtcTableMap.put("currency", currency);
		dtcTableMap.put("payCardType", payCardType);
		dtcTableMap.put("cardAmount", cardAmount);
		dtcTableMap.put("extPrice", extPrice);
		dtcTableMap.put("shipCharge", shipCharge);
		dtcTableMap.put("taxCharge", taxCharge);
		dtcTableMap.put("shipTaxTotal", shipTaxTotal);
		dtcTableMap.put("vendorId", vendorId);
		dtcTableMap.put("vendorId", vendorId);
		dtcTableMap.put("shipVia", shipVia);
		dtcTableMap.put("department", department);
		dtcTableMap.put("logo", logo);
		dtcTableMap.put("subLogo", subLogo);
		dtcTableMap.put("commitNumber", commitNumber);
		dtcTableMap.put("resvNumber", resvNumber);
		dtcTableMap.put("registNumber", registNumber);
		dtcTableMap.put("batchNumber", batchNumber);
		dtcTableMap.put("salestax", salestax);
		dtcTableMap.put("giftWrapCharge", giftWrapCharge);
		dtcTableMap.put("returnTypeCode", returnTypeCode);
		dtcTableMap.put("returnAdrCode", returnAdrCode);
		dtcTableMap.put("processYN", processYN);
		dtcTableMap.put("processTime", (processTime == null) ? null : processTime.toString());
		dtcTableMap.put("giftOrder", giftOrder);
		dtcTableMap.put("taxChargeDesc", taxChargeDesc);
		dtcTableMap.put("custServEmail", custServEmail);
		dtcTableMap.put("custServPhone", custServPhone);
		dtcTableMap.put("taxChargeB", taxChargeB);
		dtcTableMap.put("taxChargeBDesc", taxChargeBDesc);
		dtcTableMap.put("taxChargeC", taxChargeC);
		dtcTableMap.put("taxChargeCDesc", taxChargeCDesc);
		dtcTableMap.put("taxChargeD", taxChargeD);
		dtcTableMap.put("taxChargeDDesc", taxChargeDDesc);
		dtcTableMap.put("taxChargeE", taxChargeE);
		dtcTableMap.put("taxChargeEDesc", taxChargeEDesc);
		dtcTableMap.put("taxChargeF", taxChargeF);
		dtcTableMap.put("taxChargeFDesc", taxChargeFDesc);
		dtcTableMap.put("orderSubTotal", orderSubTotal);
		dtcTableMap.put("orderTotal", orderTotal);
		dtcTableMap.put("percentOff", percentOff);
		dtcTableMap.put("percentOffDesc", percentOffDesc);
		dtcTableMap.put("taxTotal", taxTotal);
		dtcTableMap.put("creditTotal", creditTotal);
		dtcTableMap.put("returnName", returnName);
		dtcTableMap.put("returnAddress1", returnAddress1);
		dtcTableMap.put("returnAddress2", returnAddress2);
		dtcTableMap.put("returnCity", returnCity);
		dtcTableMap.put("returnState", returnState);
		dtcTableMap.put("returnZip", returnZip);
		dtcTableMap.put("custField1", custField1);
		dtcTableMap.put("custField2", custField2);
		dtcTableMap.put("custField3", custField3);
		dtcTableMap.put("custField4", custField4);
		dtcTableMap.put("custField5", custField5);
		dtcTableMap.put("custField6", custField6);
		dtcTableMap.put("custField7", custField7);
		dtcTableMap.put("custField8", custField8);
		dtcTableMap.put("custField9", custField9);
		dtcTableMap.put("custField10", custField10);
		dtcTableMap.put("custField11", custField11);
		dtcTableMap.put("custField12", custField12);
		dtcTableMap.put("custField13", custField13);
		dtcTableMap.put("custField14", custField14);
		dtcTableMap.put("custField15", custField15);
		dtcTableMap.put("custField16", custField16);
		dtcTableMap.put("custField17", custField17);
		dtcTableMap.put("custField18", custField18);
		dtcTableMap.put("custField19", custField19);
		dtcTableMap.put("custField20", custField20);
		dtcTableMap.put("receiptId", receiptId);
		dtcTableMap.put("pSlipVersion", pSlipVersion);
		dtcTableMap.put("buyStore", buyStore);
	}
	
	public ArrayList<PODetail> getDetails(Connection conn, boolean loadFromProd) throws SQLException {
		
		ArrayList<PODetail> lines = new ArrayList<PODetail>();
		UtilPreparedStatement ups = null;		
		if( loadFromProd ) {
			ups = new UtilPreparedStatement(QueriesEnum.SELECT_PO_DETAIL.getQuery());
		}
		else {
			ups = new UtilPreparedStatement(QueriesEnum.SELECT_EM_DETAIL.getQuery());
		}
		
		ups.setString(1, billToId);
		ups.setString(2, customerPoNumber);
		ups.setString(3, plant);
		Statement st = conn.createStatement();
		DaUtilities.writeMessage("PODetail executing SQL: " + ups.getTranslatedSql(), false);
		ResultSet rs = st.executeQuery(ups.getTranslatedSql());
		
		while(rs.next()) {
			PODetail pod = new PODetail(billToId, customerPoNumber, plant);
			pod.loadFromResultSet(rs);
			lines.add(pod);
		}
		
		st.close();
		rs.close();
		st = null;
		rs = null;	
		
		return lines;
	}
	
	public ArrayList<POComment> getComments(Connection conn, boolean loadFromProd) throws SQLException {
		
		ArrayList<POComment> comments = new ArrayList<POComment>();
		UtilPreparedStatement ups = null;		
		if( loadFromProd ) {
			ups = new UtilPreparedStatement(QueriesEnum.SELECT_PO_COMMENT.getQuery());
		}
		else {
			ups = new UtilPreparedStatement(QueriesEnum.SELECT_EM_COMMENT.getQuery());
		}
		
		ups.setString(1, billToId);
		ups.setString(2, customerPoNumber);
		ups.setString(3, plant);
		Statement st = conn.createStatement();
		DaUtilities.writeMessage("PODetail executing SQL: " + ups.getTranslatedSql(), false);
		ResultSet rs = st.executeQuery(ups.getTranslatedSql());
		
		while(rs.next()) {
			POComment poc = new POComment(billToId, customerPoNumber, plant);
			poc.loadFromResultSet(rs);
			comments.add(poc);
		}
		
		st.close();
		rs.close();
		st = null;
		rs = null;	
		
		return comments;
	}

	public POComment getComments() {
		return null;
	}

	public String getBillToId() {
		return billToId;
	}

	public String getCustomerPoNumber() {
		return customerPoNumber;
	}

	public String getPlant() {
		return plant;
	}

	public Date getSalesOrderDate() {
		return salesOrderDate;
	}

	public String getShipToName() {
		return shipToName;
	}

	public String getShipToAddress1() {
		return shipToAddress1;
	}

	public String getShipToAddress2() {
		return shipToAddress2;
	}

	public String getShipToAddress3() {
		return shipToAddress3;
	}

	public String getShipToCity() {
		return shipToCity;
	}

	public String getShipToState() {
		return shipToState;
	}

	public String getShipToZipCode() {
		return shipToZipCode;
	}

	public String getShipToCountry() {
		return shipToCountry;
	}

	public String getShipToPhoneNumber() {
		return shipToPhoneNumber;
	}

	public String getShipToEmail() {
		return shipToEmail;
	}

	public String getPoType() {
		return poType;
	}

	public String getAmazonCustomerOrderNumber() {
		return amazonCustomerOrderNumber;
	}

	public String getBillToName() {
		return billToName;
	}

	public String getBillToAddress1() {
		return billToAddress1;
	}

	public String getBillToAddress2() {
		return billToAddress2;
	}

	public String getBillToAddress3() {
		return billToAddress3;
	}

	public String getBillToCity() {
		return billToCity;
	}

	public String getBillToState() {
		return billToState;
	}

	public String getBillToZipCode() {
		return billToZipCode;
	}

	public String getBillToCountry() {
		return billToCountry;
	}

	public String getBillToPhoneNumber() {
		return billToPhoneNumber;
	}

	public String getBillToEmail() {
		return billToEmail;
	}

	public String getMarketingInsert() {
		return marketingInsert;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public String getPromoAmount() {
		return promoAmount;
	}

	public String getShipFromWarehouse() {
		return shipFromWarehouse;
	}

	public String getCarrierType() {
		return carrierType;
	}

	public String getShipToAttention() {
		return shipToAttention;
	}

	public String getGiftWrap() {
		return giftWrap;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public String getCurrency() {
		return currency;
	}

	public String getPayCardType() {
		return payCardType;
	}

	public String getCardAmount() {
		return cardAmount;
	}

	public String getExtPrice() {
		return extPrice;
	}

	public String getShipCharge() {
		return shipCharge;
	}

	public String getTaxCharge() {
		return taxCharge;
	}

	public String getShipTaxTotal() {
		return shipTaxTotal;
	}

	public String getVendorId() {
		return vendorId;
	}

	public String getMarkFor() {
		return markFor;
	}

	public String getShipVia() {
		return shipVia;
	}

	public String getDepartment() {
		return department;
	}

	public String getLogo() {
		return logo;
	}

	public String getSubLogo() {
		return subLogo;
	}

	public String getCommitNumber() {
		return commitNumber;
	}

	public String getResvNumber() {
		return resvNumber;
	}

	public String getRegistNumber() {
		return registNumber;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public String getSalestax() {
		return salestax;
	}

	public String getGiftWrapCharge() {
		return giftWrapCharge;
	}

	public String getReturnTypeCode() {
		return returnTypeCode;
	}

	public String getReturnAdrCode() {
		return returnAdrCode;
	}

	public String getProcessYN() {
		return processYN;
	}

	public Date getProcessTime() {
		return processTime;
	}

	public String getGiftOrder() {
		return giftOrder;
	}

	public String getTaxChargeDesc() {
		return taxChargeDesc;
	}

	public String getCustServEmail() {
		return custServEmail;
	}

	public String getCustServPhone() {
		return custServPhone;
	}

	public String getTaxChargeB() {
		return taxChargeB;
	}

	public String getTaxChargeBDesc() {
		return taxChargeBDesc;
	}

	public String getTaxChargeC() {
		return taxChargeC;
	}

	public String getTaxChargeCDesc() {
		return taxChargeCDesc;
	}

	public String getTaxChargeD() {
		return taxChargeD;
	}

	public String getTaxChargeDDesc() {
		return taxChargeDDesc;
	}

	public String getTaxChargeE() {
		return taxChargeE;
	}

	public String getTaxChargeEDesc() {
		return taxChargeEDesc;
	}

	public String getTaxChargeF() {
		return taxChargeF;
	}

	public String getTaxChargeFDesc() {
		return taxChargeFDesc;
	}

	public String getOrderSubTotal() {
		return orderSubTotal;
	}

	public String getOrderTotal() {
		return orderTotal;
	}

	public String getPercentOff() {
		return percentOff;
	}

	public String getPercentOffDesc() {
		return percentOffDesc;
	}

	public String getTaxTotal() {
		return taxTotal;
	}

	public String getCreditTotal() {
		return creditTotal;
	}

	public String getReturnName() {
		return returnName;
	}

	public String getReturnAddress1() {
		return returnAddress1;
	}

	public String getReturnAddress2() {
		return returnAddress2;
	}

	public String getReturnCity() {
		return returnCity;
	}

	public String getReturnState() {
		return returnState;
	}

	public String getReturnZip() {
		return returnZip;
	}

	public String getCustField1() {
		return custField1;
	}

	public String getCustField2() {
		return custField2;
	}

	public String getCustField3() {
		return custField3;
	}

	public String getCustField4() {
		return custField4;
	}

	public String getCustField5() {
		return custField5;
	}

	public String getCustField6() {
		return custField6;
	}

	public String getCustField7() {
		return custField7;
	}

	public String getCustField8() {
		return custField8;
	}

	public String getCustField9() {
		return custField9;
	}

	public String getCustField10() {
		return custField10;
	}

	public String getCustField11() {
		return custField11;
	}

	public String getCustField12() {
		return custField12;
	}

	public String getCustField13() {
		return custField13;
	}

	public String getCustField14() {
		return custField14;
	}

	public String getCustField15() {
		return custField15;
	}

	public String getCustField16() {
		return custField16;
	}

	public String getCustField17() {
		return custField17;
	}

	public String getCustField18() {
		return custField18;
	}

	public String getCustField19() {
		return custField19;
	}

	public String getCustField20() {
		return custField20;
	}

	public String getReceiptId() {
		return receiptId;
	}

	public String getPSlipVersion() {
		return pSlipVersion;
	}

	public String getBuyStore() {
		return buyStore;
	}
	
	public boolean insert(Connection conn) {
		
		UtilPreparedStatement ups = new UtilPreparedStatement(QueriesEnum.INSERT_PO_HEADER_2.getQuery());
		
		ups.setString(1, billToId);
		ups.setString(2, customerPoNumber);
		ups.setString(3, plant);
		ups.setDate(4, salesOrderDate);
		ups.setString(5, shipToName);
		ups.setString(6, shipToAddress1);
		ups.setString(7, shipToAddress2);
		ups.setString(8, shipToAddress3);
		ups.setString(9, shipToCity);
		ups.setString(10, shipToState);
		ups.setString(11, shipToZipCode);
		ups.setString(12, shipToCountry);
		ups.setString(13, shipToPhoneNumber);
		ups.setString(14, shipToEmail);
		ups.setString(15, poType);
		ups.setString(16, amazonCustomerOrderNumber);
		ups.setString(17, billToName);
		ups.setString(18, billToAddress1);
		ups.setString(19, billToAddress2);
		ups.setString(20, billToAddress3);
		ups.setString(21, billToCity);
		ups.setString(22, billToState);
		ups.setString(23, billToZipCode);
		ups.setString(24, billToCountry);
		ups.setString(25, billToPhoneNumber);
		ups.setString(26, billToEmail);
		ups.setString(27, marketingInsert);
		ups.setString(28, promoCode);
		ups.setString(29, promoAmount);
		ups.setString(30, shipFromWarehouse);
		ups.setString(31, carrierType);
		ups.setString(32, shipToAttention);
		ups.setString(33, giftWrap);
		ups.setDate(34, modifiedDate);
		ups.setString(35, payMethod);
		ups.setString(36, currency);
		ups.setString(37, payCardType);
		ups.setString(38, cardAmount);
		ups.setString(39, extPrice);
		ups.setString(40, shipCharge);
		ups.setString(41, taxCharge);
		ups.setString(42, shipTaxTotal);
		ups.setString(43, vendorId);
		ups.setString(44, markFor);
		ups.setString(45, shipVia);
		ups.setString(46, department);
		ups.setString(47, logo);
		ups.setString(48, subLogo);
		ups.setString(49, commitNumber);
		ups.setString(50, resvNumber);
		ups.setString(51, registNumber);
		ups.setString(52, batchNumber);
		ups.setString(53, salestax);
		ups.setString(54, giftWrapCharge);
		ups.setString(55, returnTypeCode);
		ups.setString(56, returnAdrCode);
		ups.setString(57, processYN);
		ups.setTimestamp(58, processTime);
		ups.setString(59, buyStore);		
		ups.setString(60, creditTotal);
		ups.setString(61, custField1);
		ups.setString(62, custField2);
		ups.setString(63, custField3);
		ups.setString(64, custField4);
		ups.setString(65, custField5);
		ups.setString(66, custField6);
		ups.setString(67, custField7);
		ups.setString(68, custField8);
		ups.setString(69, custField9);
		ups.setString(70, custField10);
		ups.setString(71, custField11);
		ups.setString(72, custField12);
		ups.setString(73, custField13);
		ups.setString(74, custField14);
		ups.setString(75, custField15);
		ups.setString(76, custField16);
		ups.setString(77, custField17);
		ups.setString(78, custField18);
		ups.setString(79, custField19);
		ups.setString(80, custField20);
		ups.setString(81, custServEmail);
		ups.setString(82, custServPhone);
		ups.setString(83, giftOrder);
		ups.setString(84, orderSubTotal);
		ups.setString(85, orderTotal);
		ups.setString(86, percentOff);
		ups.setString(87, percentOffDesc);
		ups.setString(88, pSlipVersion);	
		ups.setString(89, receiptId);
		ups.setString(90, returnName);
		ups.setString(91, returnAddress1);
		ups.setString(92, returnAddress2);
		ups.setString(93, returnCity);
		ups.setString(94, returnState);
		ups.setString(95, returnZip);
		ups.setString(96, taxChargeB);
		ups.setString(97, taxChargeBDesc);
		ups.setString(98, taxChargeC);
		ups.setString(99, taxChargeCDesc);
		ups.setString(100, taxChargeD);
		ups.setString(101, taxChargeDDesc);		
		ups.setString(102, taxChargeDesc);			
		ups.setString(103, taxChargeE);
		ups.setString(104, taxChargeEDesc);
		ups.setString(105, taxChargeF);
		ups.setString(106, taxChargeFDesc);		
		ups.setString(107, taxTotal);
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}
}
