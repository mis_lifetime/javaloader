package constructs;

import java.sql.Connection;
import java.util.Date;

import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

public class ProcessLogEntry {
	private long processId;
	private String jobId;
	private int numFiles;
	private int numRows;
	private Date startDate;
	private Date endDate;
	
	public ProcessLogEntry(long pid, String jid) {
		processId = pid;
		jobId = jid;
		startDate = new Date();
	}
	
	public long getProcessId() {
		return processId;
	}
	public String getJobId() {
		return jobId;
	}
	
	public int getNumFiles() {
		return numFiles;
	}

	public int getNumRows() {
		return numRows;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setNumFiles(int nFiles) {
		numFiles = nFiles;
	}

	public void setNumRows(int nRows) {
		numRows = nRows;
	}

	public void setEndDate() {
		this.endDate = new Date();
	}

	public boolean startLog(Connection conn) {
		
		UtilPreparedStatement upsProcessLogInsert = new UtilPreparedStatement(
				QueriesEnum.INSERT_PROCESS_LOG.getQuery());
		upsProcessLogInsert.reset();
		upsProcessLogInsert.setLong(1, processId);
		upsProcessLogInsert.setString(2, jobId);
		upsProcessLogInsert.setTimestamp(3, new java.sql.Timestamp(startDate.getTime()));

		return DaUtilities.executeUpdate(conn, upsProcessLogInsert, true);
	}
	
	public boolean stopLog(Connection conn) {
		Date endDate = new Date();

		UtilPreparedStatement upsProcessLogUpdate = new UtilPreparedStatement(
				QueriesEnum.UPDATE_PROCESS_LOG.getQuery());
		upsProcessLogUpdate.reset();
		upsProcessLogUpdate.setInt(1, numFiles);
		upsProcessLogUpdate.setInt(2, numRows);
		upsProcessLogUpdate.setTimestamp(3, new java.sql.Timestamp(endDate
				.getTime()));
		upsProcessLogUpdate.setLong(4, processId);
		upsProcessLogUpdate.setString(5, jobId);

		return DaUtilities.executeUpdate(conn, upsProcessLogUpdate, true);
	}
}
