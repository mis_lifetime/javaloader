package constructs.EDI754;

import java.sql.Connection;
import java.util.HashMap;

import constructs.Insertable;

import utilities.DaUtilities;
import utilities.UtilPreparedStatement;
import utilities.EDI754.EDI754HeaderEnum;
import utilities.EDI754.EDI754QueriesEnum;

public class EDI754Header implements Insertable{

	HashMap<String, String> edi754HeaderMap;
	private String partnerId;
	private String partnerQual;
	private String filler;
	private String routing;
	private String routingRequestNumber;
	private String recNumber;
	private String lineNumber;
	private String transactionDate;
	private String transactionTime;
	private String contactName;
	private String phone;
	private String email;
	private String fax;
	private String vendorNumber;
	private String shipFromname;
	private String shipFromAddr1;
	private String shipFromAddr2;
	private String shipFromCity;
	private String shipFromState;
	private String shipFromPostalCode;
	
	public EDI754Header( String data ) {
		
		edi754HeaderMap = new HashMap<String, String>();
		
		for (EDI754HeaderEnum he : EDI754HeaderEnum.values()) {
			edi754HeaderMap.put(he.getTag(), he.getValueforTag(he, data));
		}
			
		setPartnerId();
		setPartnerQual();
		setFiller();
		setRouting();
		setRoutingRequestNumber();
		setRecNumber();
		setLineNumber();
		setTransactionDate();
		setTransactionTime();
		setContactName();
		setPhone();
		setEmail();
		setFax();
		setVendorNumber();
		setShipFromname();
		setShipFromAddr1();
		setShipFromAddr2();
		setShipFromCity();
		setShipFromState();
		setShipFromPostalCode();				
	}
	
	/* Getters */
	public HashMap<String, String> getEdi754HeaderMap() {
		return edi754HeaderMap;
	}
	
	public String getPartnerId() {
		return partnerId;
	}
	
	public String getPartnerQual() {
		return partnerQual;
	}
	
	public String getFiller() {
		return filler;
	}
	
	public String getRouting() {
		return routing;
	}
	
	public String getRoutingRequestNumber() {
		return routingRequestNumber;
	}
	
	public String getRecNumber() {
		return recNumber;
	}
	
	public String getLineNumber() {
		return lineNumber;
	}
	
	public String getTransactionDate() {
		return transactionDate;
	}
	
	public String getTransactionTime() {
		return transactionTime;
	}
	
	public String getContactName() {
		return contactName;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getFax() {
		return fax;
	}
	
	public String getVendorNumber() {
		return vendorNumber;
	}
	
	public String getShipFromname() {
		return shipFromname;
	}
	
	public String getShipFromAddr1() {
		return shipFromAddr1;
	}
	
	public String getShipFromAddr2() {
		return shipFromAddr2;
	}
	
	public String getShipFromCity() {
		return shipFromCity;
	}
	
	public String getShipFromState() {
		return shipFromState;
	}
	
	public String getShipFromPostalCode() {
		return shipFromPostalCode;
	}
	
	
	/* Setters (private will parse out file */
	private void setPartnerId() {
		partnerId = edi754HeaderMap.get(EDI754HeaderEnum.PARTNER_ID.getTag());
	}
	
	private void setPartnerQual() {
		partnerQual = edi754HeaderMap.get(EDI754HeaderEnum.PARTNER_QUAL.getTag());
	}
	
	private void setFiller() {
		filler = edi754HeaderMap.get(EDI754HeaderEnum.FILLER.getTag());
	}
	
	private void setRouting() {
		routing = edi754HeaderMap.get(EDI754HeaderEnum.ROUTING.getTag());
	}
	
	private void setRoutingRequestNumber() {
		routingRequestNumber = edi754HeaderMap.get(EDI754HeaderEnum.ROUTING_REQUEST_NUMBER.getTag());
	}
	
	private void setRecNumber() {
		recNumber = edi754HeaderMap.get(EDI754HeaderEnum.REC_NUMBER.getTag());
	}
	
	private void setLineNumber() {
		lineNumber = edi754HeaderMap.get(EDI754HeaderEnum.LINE_NUMBER.getTag());
	}
	
	private void setTransactionDate() {
		transactionDate = edi754HeaderMap.get(EDI754HeaderEnum.TRANSACTION_DATE.getTag());
	}
	
	private void setTransactionTime() {
		transactionTime = edi754HeaderMap.get(EDI754HeaderEnum.TRANSACTION_TIME.getTag());
	}
	
	private void setContactName() {
		contactName = edi754HeaderMap.get(EDI754HeaderEnum.CONTACT_NAME.getTag());
	}
	
	private void setPhone() {
		phone = edi754HeaderMap.get(EDI754HeaderEnum.PHONE.getTag());
	}
	
	private void setEmail() {
		email = edi754HeaderMap.get(EDI754HeaderEnum.EMAIL.getTag());
	}
	
	private void setFax() {
		fax = edi754HeaderMap.get(EDI754HeaderEnum.FAX.getTag());
	}
	
	private void setVendorNumber() {
		vendorNumber = edi754HeaderMap.get(EDI754HeaderEnum.VENDOR_NUMBER.getTag());
	}
	
	private void setShipFromname() {
		shipFromname = edi754HeaderMap.get(EDI754HeaderEnum.SHIP_FROM_NAME.getTag());
	
	}
	
	private void setShipFromAddr1() {
		shipFromAddr1 = edi754HeaderMap.get(EDI754HeaderEnum.SHIP_FROM_ADDR1.getTag());
	}
	
	private void setShipFromAddr2() {
		shipFromAddr2 = edi754HeaderMap.get(EDI754HeaderEnum.SHIP_FROM_ADDR2.getTag());
	}
	
	private void setShipFromCity() {
		shipFromCity = edi754HeaderMap.get(EDI754HeaderEnum.SHIP_FROM_CITY.getTag());
	}
	
	private void setShipFromState() {
		shipFromState = edi754HeaderMap.get(EDI754HeaderEnum.SHIP_FROM_STATE.getTag());
	}
	
	private void setShipFromPostalCode() {
		shipFromPostalCode = edi754HeaderMap.get(EDI754HeaderEnum.SHIP_FROM_POSTAL_CODE.getTag());
	}	
	
	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		
		str.append("PartnerId: " + partnerId + "\n");
		str.append("PartnerQual: " + partnerQual + "\n");
		str.append("Filler: " + filler + "\n");
		str.append("Routing: " + routing + "\n");
		str.append("RoutingRequestNumber: " + routingRequestNumber + "\n");
		str.append("RecNumber: " + recNumber + "\n");
		str.append("LineNumber: " + lineNumber + "\n");
		str.append("TransactionDate: " + transactionDate + "\n");
		str.append("TransactionTime: " + transactionTime + "\n");
		str.append("ContactName: " + contactName + "\n");
		str.append("Phone: " + phone + "\n");
		str.append("Email: " + email + "\n");
		str.append("Fax: " + fax + "\n");
		str.append("VendorNumber: " + vendorNumber + "\n");
		str.append("ShipFromname: " + shipFromname + "\n");
		str.append("ShipFromAddr1: " + shipFromAddr1 + "\n");
		str.append("ShipFromAddr2: " + shipFromAddr2 + "\n");
		str.append("ShipFromCity: " + shipFromCity + "\n");
		str.append("ShipFromState: " + shipFromState + "\n");
		str.append("ShipFromPostalCode: " + shipFromPostalCode + "\n");					
				
		return str.toString();
	}

	@Override
	public boolean insert(Connection conn) {
		
		UtilPreparedStatement ups = new UtilPreparedStatement(EDI754QueriesEnum.INSERT_EDI754HEADER.getQuery());
		ups.setString(1, partnerId);
		ups.setString(2, partnerQual);
		ups.setString(3, filler);
		ups.setString(4, routing);
		ups.setString(5, routingRequestNumber);
		ups.setString(6, recNumber);
		ups.setString(7, lineNumber);
		ups.setString(8, transactionDate);
		ups.setString(9, transactionTime);
		ups.setString(10, contactName);
		ups.setString(11, phone);
		ups.setString(12, email);
		ups.setString(13, fax);
		ups.setString(14, vendorNumber);
		ups.setString(15, shipFromname);
		ups.setString(16, shipFromAddr1);
		ups.setString(17, shipFromAddr2);
		ups.setString(18, shipFromCity);
		ups.setString(19, shipFromState);
		ups.setString(20, shipFromPostalCode);
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}
}
