package constructs.EDI754;

import java.sql.Connection;
import java.util.HashMap;

import utilities.DaUtilities;
import utilities.UtilPreparedStatement;
import utilities.EDI754.EDI754DetailEnum;
import utilities.EDI754.EDI754QueriesEnum;
import constructs.Insertable;

public class EDI754Detail implements Insertable{
	
	HashMap<String, String> edi754DetailMap;
	private String partnerId;
	private String partnerQual;
	private String filler;
	private String routing;
	private String routingRequestNumber;
	private String recNumber;
	private String lineNumber;
	private String shipToStoreNumber;
	private String routingRequestControlNumber;
	private String scacCode;
	private String pickupAppointmentNumber;
	private String scheduledPickupDate;
	private String scheduledPickupTime;
	private String multiStopShipment;
	private String stopSequenceNumber;
	private String numberTrailersForPo;
	private String transServLevelReq;	
	
	public EDI754Detail( String data ) {
		edi754DetailMap = new HashMap<String, String>();
		for (EDI754DetailEnum de : EDI754DetailEnum.values()) {
			edi754DetailMap.put(de.getTag(), de.getValueforTag(de, data));
		}
		
		setPartnerId();
		setParterQual();
		setFiller();
		setRouting();
		setRoutingRequestNumber();
		setRecNumber();
		setLineNumber();
		setShipToStoreNumber();
		setRoutingRequestControlNumber();
		setScacCode();
		setPickupAppointmentNumber();
		setScheduledPickupDate();
                setScheduledPickupTime();
		setMultiStopShipment();
		setStopSequenceNumber();
		setNumberTrailersForPo();
		setTransServLevelReq();
	}

	/* Getters */
	public HashMap<String, String> getEdi754DetailMap() {
		return edi754DetailMap;
	}


	public String getPartnerId() {
		return partnerId;
	}


	public String getParterQual() {
		return partnerQual;
	}


	public String getFiller() {
		return filler;
	}


	public String getRouting() {
		return routing;
	}


	public String getRoutingRequestNumber() {
		return routingRequestNumber;
	}


	public String getRecNumber() {
		return recNumber;
	}


	public String getLineNumber() {
		return lineNumber;
	}


	public String getShipToStoreNumber() {
		return shipToStoreNumber;
	}


	public String getRoutingRequestControlNumber() {
		return routingRequestControlNumber;
	}


	public String getScacCode() {
		return scacCode;
	}


	public String getPickupAppointmentNumber() {
		return pickupAppointmentNumber;
	}


	public String getScheduledPickupDate() {
		return scheduledPickupDate;
	}


	public String getScheduledPickupTime() {
		return scheduledPickupTime;
	}


	public String getMultiStopShipment() {
		return multiStopShipment;
	}


	public String getStopSequenceNumber() {
		return stopSequenceNumber;
	}


	public String getNumberTrailersForPo() {
		return numberTrailersForPo;
	}


	public String getTransServLevelReq() {
		return transServLevelReq;
	}

	/* Setters */
	private void setPartnerId() {
		partnerId = edi754DetailMap.get(EDI754DetailEnum.PARTNER_ID.getTag());
	}


	private void setParterQual() {
		partnerQual = edi754DetailMap.get(EDI754DetailEnum.PARTNER_QUAL.getTag());
	}


	private void setFiller() {
		filler = edi754DetailMap.get(EDI754DetailEnum.FILLER.getTag());
	}


	private void setRouting() {
		routing = edi754DetailMap.get(EDI754DetailEnum.ROUTING.getTag());
	}


	private void setRoutingRequestNumber() {
		routingRequestNumber = edi754DetailMap.get(EDI754DetailEnum.ROUTING_REQUEST_NUMBER.getTag());
	}


	private void setRecNumber() {
		recNumber = edi754DetailMap.get(EDI754DetailEnum.REC_NUMBER.getTag());
	}


	private void setLineNumber() {
		lineNumber = edi754DetailMap.get(EDI754DetailEnum.LINE_NUMBER.getTag());
	}


	private void setShipToStoreNumber() {
		shipToStoreNumber = edi754DetailMap.get(EDI754DetailEnum.SHIP_TO_STORE_NUMBER.getTag());
	}


	private void setRoutingRequestControlNumber() {
		routingRequestControlNumber = edi754DetailMap.get(EDI754DetailEnum.ROUTING_REQUEST_CONTROL_NBR.getTag());
	}


	private void setScacCode() {
		scacCode = edi754DetailMap.get(EDI754DetailEnum.SCAC_CODE.getTag());
	}


	private void setPickupAppointmentNumber() {
		pickupAppointmentNumber = edi754DetailMap.get(EDI754DetailEnum.PICKUP_APPOINTMENT_NUMBER.getTag());
	}


	private void setScheduledPickupDate() {
		scheduledPickupDate = edi754DetailMap.get(EDI754DetailEnum.SCHEDULED_PICKUP_DATE.getTag());
	}


	private void setScheduledPickupTime() {
		scheduledPickupTime = edi754DetailMap.get(EDI754DetailEnum.SCHEDULED_PICKUP_TIME.getTag());
	}


	private void setMultiStopShipment() {
		multiStopShipment = edi754DetailMap.get(EDI754DetailEnum.MULTI_STOP_SHIPMENT.getTag());
	}


	private void setStopSequenceNumber() {
		stopSequenceNumber = edi754DetailMap.get(EDI754DetailEnum.STOP_SEQUENCE_NUMBER.getTag());
	}


	private void setNumberTrailersForPo() {
		numberTrailersForPo = edi754DetailMap.get(EDI754DetailEnum.NUMBER_TRAILERS_FOR_PO.getTag());
	}


	private void setTransServLevelReq() {
		transServLevelReq = edi754DetailMap.get(EDI754DetailEnum.TRANS_SERV_LEVEL_REQ.getTag());
	}
	
	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		
		str.append("PartnerID: " + partnerId + "\n");
		str.append("PartnerQual: " + partnerQual + "\n");
		str.append("Filler: " + filler + "\n");
		str.append("Routing: " + routing + "\n");
		str.append("RoutingReq#: " + routingRequestNumber + "\n");
		str.append("Rec#: " + recNumber + "\n");
		str.append("Line#: " + lineNumber + "\n");
		str.append("ShipToStore: " + shipToStoreNumber + "\n");
		str.append("RoutingReqCtrl#: " + routingRequestControlNumber + "\n");
		str.append("SCAC: " + scacCode + "\n");
		str.append("PickupApp#: " + pickupAppointmentNumber + "\n");
		str.append("SchedPickupDte: " + scheduledPickupDate + "\n");
		str.append("SchedulePicupTime: " + scheduledPickupTime + "\n");
		str.append("MultiStopShipment: " + multiStopShipment + "\n");
		str.append("StopSeqNum: " + stopSequenceNumber + "\n");
		str.append("NumTrailersForPO: " + numberTrailersForPo + "\n");
		str.append("TransServLevelReq: " + transServLevelReq + "\n");	
				
		return str.toString();
	}

	@Override
	public boolean insert(Connection conn) {
		
		UtilPreparedStatement ups = new UtilPreparedStatement(EDI754QueriesEnum.INSERT_EDI754DETAIL.getQuery());
		ups.setString(1, partnerId);
		ups.setString(2, partnerQual);
		ups.setString(3, filler);
		ups.setString(4, routing);
		ups.setString(5, routingRequestNumber);
		ups.setString(6, recNumber);
		ups.setString(7, lineNumber);
		ups.setString(8, shipToStoreNumber);
		ups.setString(9, routingRequestControlNumber);
		ups.setString(10, scacCode);
		ups.setString(11, pickupAppointmentNumber);
		ups.setString(12, scheduledPickupDate);
		ups.setString(13, scheduledPickupTime);
		ups.setString(14, multiStopShipment);
		ups.setString(15, stopSequenceNumber);
		ups.setString(16, numberTrailersForPo);
		ups.setString(17, transServLevelReq);
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}
}
