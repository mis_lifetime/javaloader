package constructs.EDI754;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;

import utilities.DaUtilities;
import constructs.EMJob;
import constructs.ProcessLogEntry;

public class JobEDI754 extends EMJob {

	private Connection dtcConnection;
	private static String LOAD_FROM_DIR;
	private static String COPY_TO_DIR;
	private static String LOG_FILE_DIR;
	private static long procId;
	//private FileOutputStream fos = null;
	//private PrintStream ps = null;
	
	public JobEDI754() throws FileNotFoundException,
		IOException {
		super("resources/EDI754Properties");					
	}
	
	@Override
	public void postProcessEMJob() throws Exception {
		DaUtilities.writeMessage("Closing files and connections", false);
		dtcConnection.close();
		dtcConnection = null;	
		//fos.close();
		//ps.close();
		//fos = null;
		//ps = null;
		System.gc();
	}

	@Override
	public void preProcessEMJob() throws Exception {
		procId = Calendar.getInstance().getTimeInMillis();	
		LOAD_FROM_DIR = jobProperties.getProperty("INPUTDIR");
		COPY_TO_DIR = jobProperties.getProperty("OUTPUTDIR");
		LOG_FILE_DIR = jobProperties.getProperty("LOGDIR");
																			
		dtcConnection = DaUtilities.getOracleConnection(jobProperties.getProperty("CONNECTION"));
		//fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId + "_runlog.txt");
				
		//ps = new PrintStream(fos);
		//System.setOut(ps);
		//System.setErr(ps);
		
		DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);
		DaUtilities.writeMessage("COPY_TO_DIR: " + COPY_TO_DIR, false);
		DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);
		DaUtilities.writeMessage(dtcConnection.toString(), false);
	}

	@Override
	public void processEMJob() throws Exception {
		File edi754Folder = new File(LOAD_FROM_DIR);
		File copyFolder = new File(COPY_TO_DIR);
		File[] edi754Files = edi754Folder.listFiles();
		BufferedReader inputStream = null;
		int numFiles = 0;
		int numRows = 0;
		int numErrs = 0;

		DaUtilities.writeMessage("Starting Process Log", false);
		ProcessLogEntry ple = new ProcessLogEntry(procId, super.getJobId());
		ple.startLog(dtcConnection);
		
		if( edi754Files != null ) {
			for (int i = 0; i < edi754Files.length; i++) {
	
				if (!edi754Files[i].isFile())
					continue;
	
				File f = edi754Files[i];
				String origName = f.getName();
	
				if (f.renameTo(new File(COPY_TO_DIR, f.getName() + "_" + procId))) {
					DaUtilities.writeMessage("Processing File: " + origName, false);
	
					f = new File(COPY_TO_DIR, origName + "_" + procId);
					numFiles++;
					inputStream = new BufferedReader(new FileReader(copyFolder
							+ "//" + f.getName()));
	
					String line;
					while ((line = inputStream.readLine()) != null) {
	
						numRows++;					
						boolean isOk = false;
											
						String keyVal = line.substring(104, 106).trim();
						
						if(keyVal.equals("10")) {
							EDI754Header eh = new EDI754Header(line);
							eh.insert(dtcConnection);
						}
						else if(keyVal.equals("20")) {
							EDI754Detail ed = new EDI754Detail(line);
							ed.insert(dtcConnection);
						}
						else if(keyVal.equals("30")) {
							EDI754DetailOID edoid = new EDI754DetailOID(line);
							edoid.insert(dtcConnection);
						}
						else {
							DaUtilities.writeMessage("Unknown Keyval: " + keyVal, true);
						}
	
						isOk = true;
						
						numErrs += (isOk) ? 0 : 1;
					}
	
					DaUtilities.writeMessage("Closing file input stream", false);
					inputStream.close();
	
					DaUtilities.writeMessage("Renaming file back to orignal name",
							false);
					if (!f.renameTo(new File(COPY_TO_DIR, origName))) {
						DaUtilities.writeMessage(
								"Could not copy file back to original name",
								true);
					}
				} else {
					DaUtilities.writeMessage("Failed to Copy File", true);
				}
			}
		}
		
		DaUtilities.writeMessage("Completing process log.", false);
		ple.setNumFiles(numFiles);
		ple.setNumRows(numRows);
		ple.setEndDate();
		ple.stopLog(dtcConnection);
	}
}
