package constructs.EDI754;

import java.sql.Connection;
import java.util.HashMap;

import constructs.Insertable;

import utilities.DaUtilities;
import utilities.UtilPreparedStatement;
import utilities.EDI754.EDI754DetialOIDEnum;
import utilities.EDI754.EDI754QueriesEnum;

public class EDI754DetailOID implements Insertable {

	HashMap<String, String> edi754DetailOIDMap;
	private String partnerId;
	private String partnerQual;
	private String filler;
	private String routing;
	private String routingRequestNumber;
	private String recNumber;
	private String lineNumber;
	private String seqNumber;
	private String poNumber;
	private String applicationErrorCode;
	private String numberCartons;
	private String weightInPounds;
	private String cubicFeet;

	public EDI754DetailOID( String data ) {
		edi754DetailOIDMap = new HashMap<String, String>();
		for (EDI754DetialOIDEnum deoid : EDI754DetialOIDEnum.values()) {
			edi754DetailOIDMap.put(deoid.getTag(), deoid.getValueforTag(deoid, data));
		}
		
		setPartnerId();
		setPartnerQual();
		setFiller();
		setRouting();
		setRoutingRequestNumber();
		setRecNumber();
		setLineNumber();
		setSeqNumber();
		setPoNumber();
		setApplicationErrorCode();
		setNumberCartons();
		setWeightInPounds();
		setCubicFeet();
	}

	/* Getters */
	public HashMap<String, String> getEdi754DetailOIDMap() {
		return edi754DetailOIDMap;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public String getEdiPartnerQual() {
		return partnerQual;
	}

	public String getFiller() {
		return filler;
	}

	public String getRouting() {
		return routing;
	}

	public String getRoutingRequestNumber() {
		return routingRequestNumber;
	}

	public String getRecNumber() {
		return recNumber;
	}

	public String getLineNumber() {
		return lineNumber;
	}

	public String getSeqNumber() {
		return seqNumber;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public String getApplicationErrorCode() {
		return applicationErrorCode;
	}

	public String getNumberCartons() {
		return numberCartons;
	}

	public String getWeightInPounds() {
		return weightInPounds;
	}

	public String getCubicFeet() {
		return cubicFeet;
	}

	/* Setters */
	private void setPartnerId() {
		partnerId = edi754DetailOIDMap.get(EDI754DetialOIDEnum.PARTNER_ID.getTag());
	}

	private void setPartnerQual() {
		partnerQual = edi754DetailOIDMap.get(EDI754DetialOIDEnum.PARTNER_QUAL.getTag());
	}

	private void setFiller() {
		filler = edi754DetailOIDMap.get(EDI754DetialOIDEnum.FILLER.getTag());
	}

	private void setRouting() {
		routing = edi754DetailOIDMap.get(EDI754DetialOIDEnum.ROUTING.getTag());
	}

	private void setRoutingRequestNumber() {
		routingRequestNumber = edi754DetailOIDMap.get(EDI754DetialOIDEnum.ROUTING_REQUEST_NUMBER.getTag());
	}

	private void setRecNumber() {
		recNumber = edi754DetailOIDMap.get(EDI754DetialOIDEnum.REC_NUMBER.getTag());
	}

	private void setLineNumber() {
		lineNumber = edi754DetailOIDMap.get(EDI754DetialOIDEnum.LINE_NUMBER.getTag());
	}

	private void setSeqNumber() {
		seqNumber = edi754DetailOIDMap.get(EDI754DetialOIDEnum.SEQ_NUMBER.getTag());
	}

	private void setPoNumber() {
		poNumber = edi754DetailOIDMap.get(EDI754DetialOIDEnum.PO_NUMBER.getTag());
	}

	private void setApplicationErrorCode() {
		applicationErrorCode = edi754DetailOIDMap.get(EDI754DetialOIDEnum.APPLICATION_ERROR_CODE.getTag());
	}

	private void setNumberCartons() {
		numberCartons = edi754DetailOIDMap.get(EDI754DetialOIDEnum.NUMBER_CARTONS.getTag());
	}

	private void setWeightInPounds() {
		weightInPounds = edi754DetailOIDMap.get(EDI754DetialOIDEnum.WEIGHT_IN_POUNDS.getTag());
	}

	private void setCubicFeet() {
		cubicFeet = edi754DetailOIDMap.get(EDI754DetialOIDEnum.CUBIC_FEET.getTag());
	}		
	
	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		
		str.append("PartnerId: " + partnerId + "\n");
		str.append("PartnerQual: " + partnerQual + "\n");
		str.append("Filler: " + filler + "\n");
		str.append("Routing: " + routing + "\n");
		str.append("RoutingReq#: " + routingRequestNumber + "\n");
		str.append("Rec#: " + recNumber + "\n");
		str.append("Line#: " + lineNumber + "\n");
		str.append("Seq#: " + seqNumber + "\n");
		str.append("PO#: " + poNumber + "\n");
		str.append("AppErrCode: " + applicationErrorCode + "\n");
		str.append("#Cartons: " + numberCartons + "\n");
		str.append("WeightLbs: " + weightInPounds + "\n");
		str.append("CubicFeet: " + cubicFeet + "\n");			
				
		return str.toString();
	}

	@Override
	public boolean insert(Connection conn) {
		
		UtilPreparedStatement ups = new UtilPreparedStatement(EDI754QueriesEnum.INSERT_EDI754DETAILOID.getQuery());
		ups.setString(1, partnerId);
		ups.setString(2, partnerQual);
		ups.setString(3, filler);
		ups.setString(4, routing);
		ups.setString(5, routingRequestNumber);
		ups.setString(6, recNumber);
		ups.setString(7, lineNumber);
		ups.setString(8, seqNumber);
		ups.setString(9, poNumber);
		ups.setString(10, applicationErrorCode);
		ups.setString(11, numberCartons);
		ups.setString(12, weightInPounds);
		ups.setString(13, cubicFeet);
		
		return DaUtilities.executeUpdate(conn, ups, true);
	}
}
