package constructs.MATMAS;

import constructs.Insertable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import utilities.DaUtilities;
import utilities.MATMAS.MATMASQueriesEnum;
import utilities.MATMAS.MaterialMasterPartEnum;
import utilities.UtilPreparedStatement;

public class MaterialMasterPart implements Insertable {

    HashMap<String, String> partMap;
    public String fileName;

    public MaterialMasterPart(String data) {

        partMap = new HashMap<String, String>();
        for (MaterialMasterPartEnum mmp : MaterialMasterPartEnum.values()) {

            partMap.put(mmp.getTag(), mmp.getValueforTag(mmp, data));
        }
    }

    public void setFileName(String fn) {
        fileName = fn;
    }

    public String getPartNumber() {
        return partMap.get(MaterialMasterPartEnum.PART_NUMBER.getTag());
    }

    @Override
    public boolean insert(Connection conn) {
        UtilPreparedStatement ups = new UtilPreparedStatement(MATMASQueriesEnum.INSERT_MATMASPART.getQuery());

        ups.setString(1, partMap.get(MaterialMasterPartEnum.ITEM_VOLUME.getTag()));
        ups.setString(2, partMap.get(MaterialMasterPartEnum.INNER_PACK_SCC14.getTag()));
        ups.setString(3, partMap.get(MaterialMasterPartEnum.MASTER_PACK_SCC14.getTag()));
        ups.setString(4, partMap.get(MaterialMasterPartEnum.BASIC_MATERIAL.getTag()));
        ups.setString(5, partMap.get(MaterialMasterPartEnum.CATEGORY_NUM.getTag()));
        ups.setString(6, partMap.get(MaterialMasterPartEnum.CATEGORY_DESC.getTag()));
        ups.setString(7, partMap.get(MaterialMasterPartEnum.SUB_CATEGORY_NUM.getTag()));
        ups.setString(8, partMap.get(MaterialMasterPartEnum.SUB_CATEGORY_DESC.getTag()));
        ups.setString(9, partMap.get(MaterialMasterPartEnum.ITEM_TYPE_DESC.getTag()));
        ups.setString(10, partMap.get(MaterialMasterPartEnum.ITEM_GROUP_DESC.getTag()));
        ups.setString(11, partMap.get(MaterialMasterPartEnum.ITEM_CLASS_DESC.getTag()));
        ups.setString(12, partMap.get(MaterialMasterPartEnum.ITEM_GROSS_WEIGHT.getTag()));
        ups.setString(13, partMap.get(MaterialMasterPartEnum.ITEM_NET_WEIGHT.getTag()));
        ups.setString(14, partMap.get(MaterialMasterPartEnum.TAG.getTag()));
        ups.setString(15, partMap.get(MaterialMasterPartEnum.SEGMENT_ID.getTag()));
        ups.setString(16, partMap.get(MaterialMasterPartEnum.TRAN_TYPE.getTag()));
        ups.setString(17, partMap.get(MaterialMasterPartEnum.PART_NUMBER.getTag()));
        ups.setString(18, partMap.get(MaterialMasterPartEnum.PART_CLIENT_ID.getTag()));
        ups.setString(19, partMap.get(MaterialMasterPartEnum.LONG_DESC.getTag()));
        ups.setString(20, partMap.get(MaterialMasterPartEnum.UPC_CODE.getTag()));
        ups.setString(21, partMap.get(MaterialMasterPartEnum.NDC_CODE.getTag()));
        ups.setString(22, partMap.get(MaterialMasterPartEnum.COMODITY_CODE.getTag()));
        ups.setString(23, partMap.get(MaterialMasterPartEnum.SUB_CONFIG.getTag()));
        ups.setString(24, partMap.get(MaterialMasterPartEnum.SCF_POS.getTag()));
        ups.setString(25, partMap.get(MaterialMasterPartEnum.DTL_CFG.getTag()));
        ups.setString(26, partMap.get(MaterialMasterPartEnum.DCF_POS.getTag()));
        ups.setString(27, partMap.get(MaterialMasterPartEnum.TYP_COD.getTag()));
        ups.setString(28, partMap.get(MaterialMasterPartEnum.PART_FAM.getTag()));
        ups.setString(29, partMap.get(MaterialMasterPartEnum.LOAD_LEVEL.getTag()));
        ups.setString(30, partMap.get(MaterialMasterPartEnum.ORG_FLAG.getTag()));
        ups.setString(31, partMap.get(MaterialMasterPartEnum.REV_FLAG.getTag()));
        ups.setString(32, partMap.get(MaterialMasterPartEnum.LOT_FLAG.getTag()));
        ups.setString(33, partMap.get(MaterialMasterPartEnum.UNIT_COST.getTag()));
        ups.setString(34, partMap.get(MaterialMasterPartEnum.REO_QTY.getTag()));
        ups.setString(35, partMap.get(MaterialMasterPartEnum.REO_PNT.getTag()));
        ups.setString(36, partMap.get(MaterialMasterPartEnum.PACKAGE_TYPE.getTag()));
        ups.setString(37, partMap.get(MaterialMasterPartEnum.UNIT_OF_MEASURE.getTag()));
        ups.setString(38, partMap.get(MaterialMasterPartEnum.UNITS_PER_PACK.getTag()));
        ups.setString(39, partMap.get(MaterialMasterPartEnum.INNER_PACK_UOM.getTag()));
        ups.setString(40, partMap.get(MaterialMasterPartEnum.UNITS_PER_CASE.getTag()));
        ups.setString(41, partMap.get(MaterialMasterPartEnum.CASE_UOM.getTag()));
        ups.setString(42, partMap.get(MaterialMasterPartEnum.UNITS_PER_PALLET.getTag()));
        ups.setString(43, partMap.get(MaterialMasterPartEnum.PALLET_UOM.getTag()));
        ups.setString(44, partMap.get(MaterialMasterPartEnum.UNIT_LENGTH.getTag()));
        ups.setString(45, partMap.get(MaterialMasterPartEnum.INNER_PACK_LENGTH.getTag()));
        ups.setString(46, partMap.get(MaterialMasterPartEnum.GROSS_CASE_WEIGHT.getTag()));
        ups.setString(47, partMap.get(MaterialMasterPartEnum.NET_CASE_WEIGHT.getTag()));
        ups.setString(48, partMap.get(MaterialMasterPartEnum.FOOTPRINT_CODE.getTag()));
        ups.setString(49, partMap.get(MaterialMasterPartEnum.ABC_CODE.getTag()));
        ups.setString(50, partMap.get(MaterialMasterPartEnum.FIF_WIN.getTag()));
        ups.setString(51, partMap.get(MaterialMasterPartEnum.VEL_ZONE.getTag()));
        ups.setString(52, partMap.get(MaterialMasterPartEnum.RCV_INV_STATUS.getTag()));
        ups.setString(53, partMap.get(MaterialMasterPartEnum.RCV_INV_UOM.getTag()));
        ups.setString(54, partMap.get(MaterialMasterPartEnum.RCV_FLAG.getTag()));
        ups.setString(55, partMap.get(MaterialMasterPartEnum.PRD_INV_FLAG.getTag()));
        ups.setString(56, partMap.get(MaterialMasterPartEnum.LTL_CLS.getTag()));
        ups.setString(57, partMap.get(MaterialMasterPartEnum.LAST_MOD_DATE.getTag()));
        ups.setString(58, partMap.get(MaterialMasterPartEnum.LAST_MOD_USR_ID.getTag()));
        ups.setString(59, partMap.get(MaterialMasterPartEnum.BRAND.getTag()));
        ups.setString(60, partMap.get(MaterialMasterPartEnum.ITEM_TYPE.getTag()));
        ups.setString(61, partMap.get(MaterialMasterPartEnum.ITEM_GROUP.getTag()));
        ups.setString(62, partMap.get(MaterialMasterPartEnum.ITEM_CLASS.getTag()));
        ups.setString(63, partMap.get(MaterialMasterPartEnum.BRAND_DESC.getTag()));
        ups.setString(64, fileName);

        return DaUtilities.executeUpdate(conn, ups, true);
    }

    /*
     * (non-Javadoc) @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        Iterator<String> i = partMap.keySet().iterator();
        while (i.hasNext()) {
            String key = i.next();
            sb.append(key);
            sb.append(" : ");
            sb.append(partMap.get(key));
            sb.append("\n");
        }

        return sb.toString();
    }
}
