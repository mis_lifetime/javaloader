package constructs.MATMAS;

import constructs.Insertable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Iterator;
import utilities.DaUtilities;
import utilities.MATMAS.MATMASQueriesEnum;
import utilities.MATMAS.MaterialMasterFootprintEnum;
import utilities.UtilPreparedStatement;

public class MaterialMasterFootprint implements Insertable {

    HashMap<String, String> footprintMap;    
    public String fileName;
    public String partNumber;
    
    public MaterialMasterFootprint(String data) {

        footprintMap = new HashMap<String, String>();
        for (MaterialMasterFootprintEnum mmf : MaterialMasterFootprintEnum.values()) {

            footprintMap.put(mmf.getTag(), mmf.getValueforTag(mmf, data));
        }
    }

    public HashMap<String, String> getFootprintMap() {
        return footprintMap;
    }    

    public void setPartNumber(String pn) {
        partNumber = pn;
    }
    
    public void setFileName(String fn) {
        fileName = fn;
    }
    
    @Override
    public boolean insert(Connection conn) {
        UtilPreparedStatement ups = new UtilPreparedStatement(MATMASQueriesEnum.INSERT_MATMASFOOT.getQuery());

        ups.setString(1, footprintMap.get(MaterialMasterFootprintEnum.TAG.getTag()));
        ups.setString(2, footprintMap.get(MaterialMasterFootprintEnum.SEGMENT_NAME.getTag()));
        ups.setString(3, footprintMap.get(MaterialMasterFootprintEnum.TRAN_TYPE.getTag()));
        ups.setString(4, footprintMap.get(MaterialMasterFootprintEnum.LONG_DESC.getTag()));
        ups.setString(5, footprintMap.get(MaterialMasterFootprintEnum.LOCALE_ID.getTag()));
        ups.setString(6, footprintMap.get(MaterialMasterFootprintEnum.UNIT_LENGTH.getTag()));
        ups.setString(7, footprintMap.get(MaterialMasterFootprintEnum.UNIT_WIDTH.getTag()));
        ups.setString(8, footprintMap.get(MaterialMasterFootprintEnum.UNIT_HEIGHT.getTag()));
        ups.setString(9, footprintMap.get(MaterialMasterFootprintEnum.PACK_LENGTH.getTag()));
        ups.setString(10, footprintMap.get(MaterialMasterFootprintEnum.PACK_WIDTH.getTag()));
        ups.setString(11, footprintMap.get(MaterialMasterFootprintEnum.PACK_HEIGHT.getTag()));
        ups.setString(12, footprintMap.get(MaterialMasterFootprintEnum.CASE_LENGTH.getTag()));
        ups.setString(13, footprintMap.get(MaterialMasterFootprintEnum.CASE_WIDTH.getTag()));
        ups.setString(14, footprintMap.get(MaterialMasterFootprintEnum.CASE_HEIGHT.getTag()));
        ups.setString(15, footprintMap.get(MaterialMasterFootprintEnum.CASE_LEVEL.getTag()));
        ups.setString(16, footprintMap.get(MaterialMasterFootprintEnum.MOD_DATE.getTag()));
        ups.setString(17, footprintMap.get(MaterialMasterFootprintEnum.MOD_USR_ID.getTag()));                
        ups.setString(18, partNumber);
        ups.setString(19, fileName);                

        return DaUtilities.executeUpdate(conn, ups, true);
    }

    /*
     * (non-Javadoc) @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();

        Iterator<String> i = footprintMap.keySet().iterator();
        while (i.hasNext()) {
            String key = i.next();
            sb.append(key);
            sb.append(" : ");
            sb.append(footprintMap.get(key));
            sb.append("\n");
        }

        return sb.toString();
    }
}
