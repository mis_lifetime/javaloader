package constructs.MATMAS;

import constructs.EMJob;
import constructs.ProcessLogEntry;
import java.io.*;
import java.sql.Connection;
import java.util.Calendar;
import utilities.DaUtilities;

public class JobMATMAS extends EMJob {

    private Connection dtcConnection;
    private static String LOAD_FROM_DIR;
    private static String COPY_TO_DIR;
    private static String LOG_FILE_DIR;
    private static long procId;

    public JobMATMAS() throws FileNotFoundException,
            IOException {
        super("resources/MATMASProperties");
    }

    @Override
    public void postProcessEMJob() throws Exception {
        DaUtilities.writeMessage("Closing files and connections", false);
        dtcConnection.close();
        dtcConnection = null;
        //fos.close();
        //ps.close();
        //fos = null;
        //ps = null;
        System.gc();
    }

    @Override
    public void preProcessEMJob() throws Exception {
        procId = Calendar.getInstance().getTimeInMillis();
        LOAD_FROM_DIR = jobProperties.getProperty("INPUTDIR");
        COPY_TO_DIR = jobProperties.getProperty("OUTPUTDIR");
        LOG_FILE_DIR = jobProperties.getProperty("LOGDIR");

        dtcConnection = DaUtilities.getOracleConnection(
                jobProperties.getProperty("CONNECTION"));
        //fos = new FileOutputStream(LOG_FILE_DIR + "\\" + procId + "_runlog.txt");

        //ps = new PrintStream(fos);
        //System.setOut(ps);
        //System.setErr(ps);

        DaUtilities.writeMessage("LOAD_FROM_DIR: " + LOAD_FROM_DIR, false);
        DaUtilities.writeMessage("COPY_TO_DIR: " + COPY_TO_DIR, false);
        DaUtilities.writeMessage("LOG_FILE_DIR: " + LOG_FILE_DIR, false);
        DaUtilities.writeMessage(dtcConnection.toString(), false);
    }

    @Override
    public void processEMJob() throws Exception {
        File matMasFolder = new File(LOAD_FROM_DIR);
        File copyFolder = new File(COPY_TO_DIR);
        File[] matMasFiles = matMasFolder.listFiles();
        BufferedReader inputStream = null;
        int numFiles = 0;
        int numRows = 0;
        int numErrs = 0;

        DaUtilities.writeMessage("Starting Process Log", false);
        ProcessLogEntry ple = new ProcessLogEntry(procId, super.getJobId());
        ple.startLog(dtcConnection);

        for (int i = 0; i < matMasFiles.length; i++) {

            if (!matMasFiles[i].isFile()) {
                continue;
            }

            File f = matMasFiles[i];            
            String origName = f.getName();
            
            if (f.getName().startsWith("MATMAS")) {                                              
                
                DaUtilities.writeMessage("Processing File: " + f.getAbsolutePath(), false);
                if (f.renameTo(new File(COPY_TO_DIR, f.getName() + "_" + procId))) {                    

                    f = new File(COPY_TO_DIR, origName + "_" + procId);
                    numFiles++;
                    inputStream = new BufferedReader(new FileReader(copyFolder
                            + "//" + f.getName()));

                    String line;
                    while ((line = inputStream.readLine()) != null) {

                        numRows++;
                        boolean isOk = false;

                        String keyVal = line.substring(0, 20).trim();

                        if (keyVal.equals("ITEM_FOOTPRINT_TRAN")) {

                            MaterialMasterFootprint mmf = null;
                            MaterialMasterPart mmp = null;
                            String partNumber = null;
                            for (int ivar = 0; ivar < 2; ivar++) {

                                line = inputStream.readLine();
                                keyVal = line.substring(0, 20).trim();

                                if (keyVal.equals("FOOTPRINT")) {
                                    mmf = new MaterialMasterFootprint(line);
                                } else if (keyVal.equals("PART")) {
                                    mmp = new MaterialMasterPart(line);
                                    partNumber = mmp.getPartNumber();
                                }
                            }

                            mmf.setPartNumber(partNumber);
                            mmf.setFileName(origName);
                            mmp.setFileName(origName);
                            mmf.insert(dtcConnection);
                            mmp.insert(dtcConnection);
                        }

                        isOk = true;

                        numErrs += (isOk) ? 0 : 1;
                    }

                    DaUtilities.writeMessage("Closing file input stream", false);
                    inputStream.close();

                    DaUtilities.writeMessage("Renaming file back to orignal name",
                            false);
                    if (!f.renameTo(new File(COPY_TO_DIR, origName))) {
                        DaUtilities.writeMessage(
                                "Could not copy file back to original name",
                                true);
                    }
                } else {
                    DaUtilities.writeMessage("Failed to Copy File", true);
                }
            }            
        }

        DaUtilities.writeMessage("Completing process log.", false);
        ple.setNumFiles(numFiles);
        ple.setNumRows(numRows);
        ple.setEndDate();
        ple.stopLog(dtcConnection);
    }
}
